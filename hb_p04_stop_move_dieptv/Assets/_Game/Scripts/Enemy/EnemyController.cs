using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class EnemyController : Character
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float stopThreshold;

    private float _groundLength,_groundWidth;
    private NavMeshAgent _navMeshAgent;
    private IState currentState;

    private bool _isAttacking = false, _isMoving = false;
    private Vector3 _destination;
    private Sequence _patrolSequence;
    public bool IsAttacking { 
        get => _isAttacking;
        set => _isAttacking = value;
    }

    [field: SerializeField] public bool IsAttack { get; private set; }

    public NavMeshAgent m_NahMeshAgent => _navMeshAgent;
    public float StopThreshold => stopThreshold;

    protected override void Awake()
    {
        base.Awake();
        seekDetector.Type = DetectorType.Seek;
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.speed = moveSpeed;
        //_groundLength = LevelManager.Instance.CurrentLevel.GroundLength;
        //_groundWidth = LevelManager.Instance.CurrentLevel.GroundWidth;

        _groundLength = 50f;
        _groundWidth = 30;
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        Init();

        LevelManager.Instance.ABC();
    }
    private void Update()
    {
        if (currentState != null)
        {
            currentState.OnExecute(this);
        }

        if(_isMoving && RemainDistance() < stopThreshold && !_navMeshAgent.pathPending)
        {
            StopMoving(true);
        }
    }

    private void Init()
    {
        ChangeState(new IdleState());
    }

    public void ChangeState(IState newState)
    {
        if (currentState != null)
        {
            currentState.OnExit(this);
        }

        currentState = newState;
        //Debug.Log(newState.ToString());
        if (currentState != null)
        {
            currentState.OnEnter(this);
        }
    }

    public void StopMoving(bool setIdle)
    {
        if (_navMeshAgent.enabled)
        {
            if (setIdle)
            {
                ChangeAnim(Constant.idleAnimName);
            }
            _navMeshAgent.isStopped = true;
            _isMoving = false;
            _navMeshAgent.enabled = false;
        }
    }


    public void FindAndGoToNextTarget()
    {
        if (seekDetector.IsEmpty())
        {
            Vector3 newRandomPosition = new Vector3(UnityEngine.Random.Range(-1 * _groundLength / 2f, _groundLength / 2f),
                                                    _tf.position.y,
                                                    UnityEngine.Random.Range(-1 * _groundWidth / 2f, _groundWidth / 2f));
            GoTo(newRandomPosition);
        }
        else
        {
            GoTo(seekDetector.GetClosetEnemy().Tf.position);
        }
    }


    public void GoTo(Vector3 position)
    {
        _destination = position;
        _navMeshAgent.enabled = true;
        _navMeshAgent.SetDestination(position);

        _navMeshAgent.isStopped = false;
        ChangeAnim(Constant.runAnimName);
        _isMoving = true;
    }

    public float RemainDistance()
    {
        return Vector3.Distance(_tf.position, _destination);
    }

    public override void OnVictory(Transform centerPoint)
    {
        ChangeState(new VictoryState());
        _navMeshAgent.speed = 0;
        _navMeshAgent.isStopped = true;
        base.OnVictory(centerPoint);
    }

    public override void OnFoundEnemyInAttackRange()
    {
        base.OnFoundEnemyInAttackRange();
        if (!_isAttacking)
        {
            _isAttacking = true;
            StopMoving(true);
            DOTween.Sequence()
                .AppendInterval(0.5f)
                .AppendCallback(() =>
                {
                    if (!attackDetector.IsEmpty())
                    {
                        ChangeState(new AttackState());
                    }
                    else
                    {
                        _isAttacking = false;
                        FindAndGoToNextTarget();
                    }
                });
        }

    }

    public void StartPatrolLoop()
    {
        _patrolSequence = DOTween.Sequence()
        .AppendInterval(2f)
        .AppendCallback(() =>
        {
            if (!_isAttacking || seekDetector.IsEmpty())
            {
                FindAndGoToNextTarget();
            }
        })
        .SetLoops(-1, LoopType.Restart);
    }

    public void StopPatrolLoop()
    {
        if(_patrolSequence != null)
        {
            _patrolSequence.Kill();
        }
    }

    public override bool IsEnemy()
    {
        return true;
    }

    public bool HasEnemyInArea()
    {
        return !seekDetector.IsEmpty();
    }

    protected override void Die()
    {
        ChangeState(new DeathState());
        base.Die();
    }
}
