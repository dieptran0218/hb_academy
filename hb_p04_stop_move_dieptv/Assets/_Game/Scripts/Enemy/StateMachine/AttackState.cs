﻿using UnityEngine;

public class AttackState : IState
{
    float animTime;
    float timer;
    public void OnEnter(EnemyController enemy)
    {
        enemy.Attack();
        animTime = 0.3f;
        timer = 0f;
    }

    public void OnExecute(EnemyController enemy)
    {
        if (timer > animTime)
        {
            enemy.ChangeState(new PatrolState());
            enemy.IsAttacking = false;
        }
        else
        {
            timer += Time.deltaTime;
        }
    }

    public void OnExit(EnemyController enemy)
    {

    }
}
