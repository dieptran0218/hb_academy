using UnityEngine;

public class PatrolState : IState
{
    public void OnEnter(EnemyController enemy)
    {
        //enemy.StackToLay = Random.Range(3, 5);
        enemy.FindAndGoToNextTarget();
        enemy.StartPatrolLoop();
    }

    public void OnExecute(EnemyController enemy)
    {
        if (enemy.RemainDistance() < enemy.StopThreshold)
        {
            if (enemy.HasEnemyInArea())
            {
                enemy.ChangeState(new IdleState());
            }
            else
            {
                enemy.FindAndGoToNextTarget();
            }
        }
        
    }

    public void OnExit(EnemyController enemy)
    {
        enemy.StopPatrolLoop();
    }
}
