using UnityEngine;

public class IdleState : IState
{
    float randomTime;
    float timer;
    public void OnEnter(EnemyController enemy)
    {
        enemy.StopMoving(true);
        timer = 0;
        randomTime = Random.Range(1f, 2f);
    }

    public void OnExecute(EnemyController enemy)
    {
        if (timer > randomTime)
        {
            enemy.ChangeState(new PatrolState());
        }
        else
        {
            timer += Time.deltaTime;
        }
    }

    public void OnExit(EnemyController enemy)
    {

    }
}
