using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundType { Jump = 0, Run = 1, Idle = 2, }


public class SoundManager : MonoBehaviour
{
    public List<AudioClip> listSFX;
    public List<AudioSource> listAudioSource;

    public static SoundManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void PlaySound(SoundType soundType)
    {
        //listSFX[(int)soundType];
    }


    public void PlaySound(string clipName, string audioSourceName, bool isLoop = false, float delay = 0f)
    {
        int audioSourceIndex = -1;
        for (int i = 0; i < listAudioSource.Count; i++)
        {
            if (audioSourceName == listAudioSource[i].name)
            {
                audioSourceIndex = i;
            }
        }

        if (audioSourceIndex == -1)
        {
            Debug.Log("AudioSource not found: " + audioSourceName);
            return;
        }

        for (int i = 0; i < listSFX.Count; i++)
        {
            if (clipName == listSFX[i].name)
            {
                listAudioSource[audioSourceIndex].loop = isLoop;
                listAudioSource[audioSourceIndex].clip = listSFX[i];

                if (delay > 0)
                {
                    listAudioSource[audioSourceIndex].PlayDelayed(delay);
                }
                else
                {
                    listAudioSource[audioSourceIndex].Play();
                }
                return;
            }
        }
        Debug.Log("AudioClip not found: " + clipName);
    }

    public void StopPlaying(string audioSourceName)
    {
        int audioSourceIndex = -1;
        for (int i = 0; i < listAudioSource.Count; i++)
        {
            if (audioSourceName == listAudioSource[i].name)
            {
                listAudioSource[i].Stop();
            }
        }

        if (audioSourceIndex == -1)
        {
            //Debug.Log("AudioSource not found: " + audioSourceName);
            return;
        }
    }

    public void InvokePlaySound(string clipName, string audioSourceName, bool isLoop = false, float delay = 0f)
    {
        StartCoroutine(PlaySoundInvoke(clipName, audioSourceName, isLoop, delay));
    }

    IEnumerator PlaySoundInvoke(string clipName, string audioSourceName, bool isLoop = false, float delay = 0f)
    {
        yield return new WaitForSeconds(delay);
        PlaySound(clipName, audioSourceName, isLoop, 0f);
    }
}
