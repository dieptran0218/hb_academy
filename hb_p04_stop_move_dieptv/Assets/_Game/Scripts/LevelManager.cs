using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> levelList;
    private int _currentLevelIndex;
    [SerializeField] private LevelController _currentLevel;

    public LevelController CurrentLevel => _currentLevel;

    private static LevelManager ins;
    public static LevelManager Instance { 
        get 
        {
            if (ins == null)
            {
                ins = FindObjectOfType<LevelManager>();
                if (ins == null)
                {
                    ins = new GameObject().AddComponent<LevelManager>();
                }
            }
            return ins;
        }
    }

    private void Awake()
    {
        //if(Instance == null || Instance != this)
        //{
        //    Instance = this;
        //}
    }


    // Start is called before the first frame update
    void Start()
    {
        _currentLevel.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void ABC()
    {
    }

    int currentLevel;

    //LevelController levelController;
    ////TODO 
    //public void LoadLevel(int level)
    //{
    //    if (levelController != null)
    //    {
    //        GameObject.Destroy(levelController.gameObject);
    //    }

    //    levelController = levelList[level];

    //    if (levelController != null)
    //    {
    //        levelController.OnInit();
    //    }
    //}
}
