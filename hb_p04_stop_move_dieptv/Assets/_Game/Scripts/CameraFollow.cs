using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //private bool _isPlayerAttach = true;
    //private Quaternion _originalRotation;
    [SerializeField] Transform target;
    [SerializeField] Vector3 offset;
    [SerializeField] Vector3 victoryOffset;
    [SerializeField] float moveSpeed;
    private Transform _tf;

    public static CameraFollow Instance;

    private void Awake()
    {
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }
        _tf = transform;
        //_originalRotation = transform.rotation;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //if (_isPlayerAttach)
        //{
        //    transform.rotation = _originalRotation;
        //}
        _tf.position = Vector3.Lerp(_tf.position, target.position + offset, moveSpeed * Time.fixedDeltaTime);
    }

    public void OnVictory(Transform newTarget)
    {
        target = newTarget;
        offset = victoryOffset;
    }
}
