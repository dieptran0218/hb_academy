using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private Transform ground;
    private float _groundWidth = 0;
    private float _groundLength = 0;

    public float GroundWidth => _groundWidth;
    public float GroundLength => _groundLength;

    private void Awake()
    {
        _groundWidth = ground.localScale.x;
        _groundLength = ground.localScale.z;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 GetRandomPoint()
    {
        return Vector3.zero;
    }
}
