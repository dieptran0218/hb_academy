using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    [SerializeField] private GameObject prefab;

    private Queue<GameObject> objectQueue;
    private Transform _tf;

    public static PoolManager Instance;

    private void Awake()
    {
        _tf = transform;
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }
        objectQueue = new Queue<GameObject>();
    }

    public GameObject GetObject()
    {
        if(objectQueue.Count > 0)
        {
            return objectQueue.Dequeue();
        }
        else
        {
            return Instantiate(prefab,Vector3.zero,Quaternion.identity, _tf);
        }

        // Not init yet
    }

    public void ReturnPool(GameObject projectile)
    {
        projectile.gameObject.SetActive(false);
        objectQueue.Enqueue(projectile);
    }
}
