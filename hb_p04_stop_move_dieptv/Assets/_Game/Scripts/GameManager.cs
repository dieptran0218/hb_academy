﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState { MainMenu, GamePlay, Finish }

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Character> characterList;
    public List<Character> CharacterList => characterList;
    public static GameManager Instance;

    private void Awake()
    {
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }
    }

    GameState state;

    public void ChangeState(GameState state) => this.state = state;

    public bool IsState(GameState state) => this.state == state;

}
