using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DetectorType {
    Attack = 0,
    Seek = 1
}

public class EnemyDetectorController : MonoBehaviour
{
    [SerializeField] private Character _owner;
    private Transform _tf;
    private List<Character> _characterInRangeList;
    private bool _isOwnerEnemy = false;
    private DetectorType _type;

    public DetectorType Type
    {
        get { return _type; }
        set { _type = value; }
    }
    private void Awake()
    {
        _characterInRangeList = new List<Character>();
        _isOwnerEnemy = _owner.IsEnemy();
        _tf = transform;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(Constant.playerTag) || other.CompareTag(Constant.enemyTag))
        {
            Character target = other.GetComponent<Character>();
            _characterInRangeList.Add(target);


            switch (_type)
            {
                case DetectorType.Attack:
                    target.AddAttackingEnemy(_owner);
                    break;
                case DetectorType.Seek:
                    target.AddSeekingEnemy(_owner);
                    break;
            }

            if (_isOwnerEnemy)
            {
                _owner.OnFoundEnemyInAttackRange();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(Constant.playerTag) || other.CompareTag(Constant.enemyTag))
        {
            Character target = other.GetComponent<Character>();
            _characterInRangeList.Remove(target);

            switch (_type)
            {
                case DetectorType.Attack:
                    target.RemoveAttackingEnemy(_owner);
                    break;
                case DetectorType.Seek:
                    target.RemoveSeekingEnemy(_owner);
                    break;
            }
        }
    }

    public Character GetClosetEnemy()
    {
        if (_characterInRangeList.Count <= 0) return null;

        Character closetEnemy = _characterInRangeList[0];
        float minDistance = Vector3.Distance(_tf.position,closetEnemy.Tf.position);

        for(int i=1; i < _characterInRangeList.Count; ++i)
        {
            float distance = Vector3.Distance(_tf.position, _characterInRangeList[i].Tf.position);
            if(minDistance > distance)
            {
                minDistance = distance;
                closetEnemy = _characterInRangeList[i];
            }
        }
        return closetEnemy;
    }

    internal bool IsEmpty()
    {
        return _characterInRangeList.Count == 0;
    }

    public void AddTarget(Character target)
    {
        _characterInRangeList.Add(target);
    }

    public void RemoveTarget(Character target)
    {
        _characterInRangeList.Remove(target);
    }
}
