﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerController : MonoBehaviour
{
    [SerializeField] private float moveSpeed, rotateSpeed;
    [SerializeField] private VariableJoystick variableJoystick;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float moveThreshold;

    private PlayerController _player;
    private Transform _tf;

    private void Awake()
    {
        _player = GetComponent<PlayerController>();
        _tf = _player.Tf;
    }

    public void FixedUpdate()
    {
        if (!_player.CanMove) return;
        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        //rb.velocity = direction * speed;
        //rb.rotation = Quaternion.Euler(direction).;

        if(Vector3.Distance(direction, Vector3.zero) > moveThreshold)
        {
            _player.OnMove();
        }
        else
        {
            _player.OnStop();
        }

        _tf.Translate(direction * moveSpeed * Time.fixedDeltaTime, Space.World);

        //chỉnh hướng mặt player
        if(direction != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(direction, Vector3.up);
            _tf.rotation = Quaternion.RotateTowards(_tf.rotation, toRotation, rotateSpeed * Time.fixedDeltaTime);
        }

        // reset Rotation Camera?

    }


}