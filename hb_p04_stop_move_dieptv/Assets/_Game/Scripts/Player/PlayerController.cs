using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : Character
{
    private float _minZ = float.MinValue;
    private float _bridgeStepLimitZ = float.MaxValue;
    private bool _canMove = true;
    private bool _isRunning = false;

    public bool CanMove => _canMove;

    public float MinZ
    {
        get => _minZ;
        set => _minZ = value;
    }
    public float BridgeStepLimitZ
    {
        get => _bridgeStepLimitZ;
        set => _bridgeStepLimitZ = value;
    }

    public void OnMove()
    {
        if (!_isRunning)
        {
            _isRunning = true;
            ChangeAnim(Constant.runAnimName);
        }
    }

    public void OnStop()
    {
        if (_isRunning)
        {
            _isRunning = false;
            ChangeAnim(Constant.idleAnimName);
            Attack();
        }

    }

    public override void OnVictory(Transform centerPoint)
    {
        _canMove = false;
        base.OnVictory(centerPoint);
    }
}
