using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    [SerializeField] private Material redMaterial, greenMaterial, blueMaterial, yellowMaterial;
    [SerializeField] private SkinnedMeshRenderer meshRenderer;
    [SerializeField] private Animator anim;
    [SerializeField] protected EnemyDetectorController attackDetector;
    [SerializeField] protected EnemyDetectorController seekDetector;
    [SerializeField] private Transform firingPoint;

    [SerializeField] private float projectileSpeed;

    private List<Character> _attackingEnemy = new List<Character>(); //a list of characters that each one has this in attack range
    private List<Character> _seekingEnemy = new List<Character>();//a list of characters that each one has this in seek range

    protected Transform _tf;
    public Transform Tf { 
        get 
        {
            if (_tf == null)
            {
                _tf = transform;
            }
            return _tf;
        }
    }

    public float ProjectileSpeed => projectileSpeed;
    public Transform FiringPoint => firingPoint;
    private string currentAnim = "Idle";

    protected virtual void Awake()
    {
        _tf = transform;
        attackDetector.Type = DetectorType.Attack;
        //_attackingEnemy = new List<Character>();
        //_seekingEnemy = new List<Character>();
    }

    protected virtual void Start()
    {
    }

    public virtual void OnVictory(Transform centerPoint)
    {
        CameraFollow.Instance.OnVictory(_tf);
        //TODO: cache transform
        //clockwatch
        ChangeAnim(Constant.runAnimName);
        DOTween.Sequence()
            .Append(_tf.DOMoveX(centerPoint.position.x, 0.3f).SetEase(Ease.Linear))
            .Join(_tf.DOMoveZ(centerPoint.position.z, 0.3f).SetEase(Ease.Linear))
            .AppendCallback(()=> {
                ChangeAnim(Constant.danceAnimName);
            })
            .Append(_tf.DORotate(new Vector3(0, 180, 0), 1f).SetEase(Ease.Linear));
    }

    //protected void ChangeAnim(string animName)
    //{
    //    if (!anim.GetCurrentAnimatorClipInfo(0)[0].clip.name.Equals(animName))
    //    {
    //        currentAnim = animName;
    //        anim.ResetTrigger(animName);
    //        anim.SetTrigger(animName);
    //    }
    //}   
    protected void ChangeAnim(string animName)
    {
        if (currentAnim != animName)
        {
            anim.ResetTrigger(currentAnim);
            currentAnim = animName;
            anim.SetTrigger(currentAnim);
        }
    }

    public void Attack()
    {
        Character closetEnemy = attackDetector.GetClosetEnemy();
        if(closetEnemy != null)
        {
                //TODO: cache getcomponent -> generic
             ProjectileController newProjectile = PoolManager.Instance.GetObject().GetComponent<ProjectileController>();

            newProjectile.Init(this, closetEnemy);
        }

    }

    public void OnGetHit()
    {
        Die();
    }

    protected virtual void Die()
    {
        //TODO: neu co the thi dung for thay foreach
        foreach(Character enemy in _attackingEnemy)
        {
            enemy.attackDetector.RemoveTarget(this);
        }

        foreach (Character enemy in _seekingEnemy)
        {
            if (enemy.seekDetector != null)
            {
                enemy.seekDetector.RemoveTarget(this);
            }
        }

        gameObject.SetActive(false);

    }

    public virtual void OnFoundEnemyInAttackRange() { }

    public void AddSeekingEnemy(Character enemy)
    {
        _seekingEnemy.Add(enemy);
    }

    public void RemoveSeekingEnemy(Character enemy)
    {
        _seekingEnemy.Remove(enemy);
    }

    public void AddAttackingEnemy(Character enemy)
    {
        _attackingEnemy.Add(enemy);
    }


    public void RemoveAttackingEnemy(Character enemy)
    {
        _attackingEnemy.Remove(enemy);
    }

    public virtual bool IsEnemy()
    {
        return false;
    }
}
