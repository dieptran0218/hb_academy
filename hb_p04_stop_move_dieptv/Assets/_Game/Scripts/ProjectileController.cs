using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ProjectileController : MonoBehaviour
{
    [SerializeField] private float flyDistance;
    private float _timeTravel;
    private Character _owner;
    private Tween _rotateTween;
    private Tween _moveTween;
    private Transform _tf;

    public void Init(Character newOwner, Character target)
    {
        _owner = newOwner;
        if (_tf == null) _tf = transform;
        _tf.position = newOwner.FiringPoint.position;
        _timeTravel = 10f / newOwner.ProjectileSpeed;
        Vector3 distance = target.Tf.position - newOwner.Tf.position;
        _tf.eulerAngles = new Vector3(0,Mathf.Atan(distance.x / distance.z) * Mathf.Rad2Deg,0);
        if(distance.z > 0)
        {
            _tf.eulerAngles = _tf.eulerAngles - Vector3.up * 180f;
        }
        distance = Vector3.Normalize(distance) * flyDistance;
        gameObject.SetActive(true);
        _moveTween = _tf.DOMove(_tf.position + distance, _timeTravel).OnComplete(Destroy);
        _rotateTween = _tf.DOLocalRotate(_tf.localRotation.eulerAngles + Vector3.left * 360f, 0.5f, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(Constant.playerTag) || other.CompareTag(Constant.enemyTag))
        {
            Character target = other.GetComponent<Character>();
            if(target != _owner)
            {
                target.OnGetHit();
                Destroy();
            }
        }
        else if (other.CompareTag("Wall"))
        {
            Destroy();
        }
    }

    private void Destroy()
    {
        _moveTween.Kill();
        _rotateTween.Kill();
        PoolManager.Instance.ReturnPool(gameObject);
    }
}
