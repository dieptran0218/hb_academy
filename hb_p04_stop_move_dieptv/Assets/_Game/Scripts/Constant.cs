using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant
{
    public static string runAnimName = "Run";
    public static string danceAnimName = "Dance";
    public static string idleAnimName = "Idle";
    public static string playerTag = "Player";
    public static string enemyTag = "Enemy";
    public static string wallTag = "Wall";
}
