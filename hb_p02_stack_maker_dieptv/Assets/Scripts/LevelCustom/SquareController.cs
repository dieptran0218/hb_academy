using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SquareController : MonoBehaviour
{
    public Image img;
    public CubeType _cubeType;
    //private Vector2 _index = new Vector2();

    //public Vector2 Index
    //{
    //    get => _index;
    //    set => _index = value;
    //}

    private Color32 startColor = new Color32(96,253,56,255);
    private Color32 finishColor = new Color32(219, 90, 90, 255);
    private Color32 stackableColor = new Color32(231, 217, 36, 255);
    private Color32 unstackableColor = new Color32(135, 135, 135, 255);
    private Color32 groundColor = new Color32(255, 255, 255, 255);
    // Start is called before the first frame update

    private void Start()
    {
        img = GetComponent<Image>();
    }

    public void MouseDrag()
    {
        if (!LevelCustomController.Instance.IsDragging) return;
        Debug.Log("Drag");
        

        if (_cubeType != LevelCustomController.Instance.PointerType)
        {
            _cubeType = LevelCustomController.Instance.PointerType;
            UpdateColor();
        }
                
    }

    public void MouseDown()
    {
        Debug.Log("down");
        LevelCustomController.Instance.IsDragging = true;   
        if (_cubeType != LevelCustomController.Instance.PointerType)
        {
            _cubeType = LevelCustomController.Instance.PointerType;
            UpdateColor();
        }

    }

    public void MouseUp()
    {
        Debug.Log("up");
        LevelCustomController.Instance.IsDragging = false;
    }

    public void SetType(CubeType type)
    {
        _cubeType = type;
        UpdateColor();
    }

    private void UpdateColor()
    {
        switch (_cubeType)
        {
            case CubeType.Ground:
                img.color = groundColor;
                break;
            case CubeType.Stackable:
                img.color = stackableColor;
                break;

            case CubeType.Unstackable:
                img.color = unstackableColor;
                break;

            case CubeType.Start:
                img.color = startColor;
                break;
            case CubeType.Finish:
                img.color = finishColor;
                break;
        }
    }

}
