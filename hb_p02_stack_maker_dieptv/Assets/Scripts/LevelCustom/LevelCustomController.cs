using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.IO;
using TMPro;

public class LevelCustomController : MonoBehaviour
{
    [SerializeField] private Transform arrowImage,startBtn,finishBtn,stackBtn,unstackBtn,groundBtn;
    [SerializeField] private GameObject settingPanel;
    [SerializeField] private TMP_InputField rowSizeInput;
    [SerializeField] private TMP_InputField columnSizeInput;
    [SerializeField] private GameObject squarePrefab;
    [SerializeField] private Transform squareParent;

    private int _availableLevel;
    private MapProperty _mapProperty;
    private SquareController[,] squareMatrix;
    private CubeType _pointerType = CubeType.Stackable;
    private bool _isDragging = false;
    public CubeType PointerType => _pointerType;

    public bool IsDragging {
        get => _isDragging;
        set => _isDragging = value;
    }

    public static LevelCustomController Instance;


    private void Start()
    {
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }

        Debug.Log("Available level: " + (_availableLevel = AvailableLevelCount()));
    }

    public void OnMouseDownSquare()
    {
        _isDragging = true;
    }

    public void OnMouseUpSquare()
    {
        _isDragging = false;
    }

    public void OnGetInputType(int type)
    {
        switch (type)
        {
            case (int)CubeType.Start:
                _pointerType = CubeType.Start;
                arrowImage.DOLocalMoveX(startBtn.localPosition.x, 0.5f);
                break;
            case (int)CubeType.Finish:
                _pointerType = CubeType.Finish;
                arrowImage.DOLocalMoveX(finishBtn.localPosition.x, 0.5f);
                break;
            case (int)CubeType.Stackable:
                _pointerType = CubeType.Stackable;
                arrowImage.DOLocalMoveX(stackBtn.localPosition.x, 0.5f);
                break;
            case (int)CubeType.Unstackable:
                _pointerType = CubeType.Unstackable;
                arrowImage.DOLocalMoveX(unstackBtn.localPosition.x, 0.5f);
                break;
            case (int)CubeType.Ground:
                _pointerType = CubeType.Ground;
                arrowImage.DOLocalMoveX(groundBtn.localPosition.x, 0.5f);
                break;
        }
    }

    public void OnSettingPanelClicked()
    {
        settingPanel.SetActive(true);
    }

    public void OnSaveSettingClicked()
    {
        _mapProperty = new MapProperty();
        _mapProperty.Row = int.Parse(rowSizeInput.text);
        _mapProperty.Column = int.Parse(columnSizeInput.text);
        _mapProperty.Matrix = new int[_mapProperty.Row, _mapProperty.Column];

        //man hinh doc
        if(Screen.width < Screen.height)
        {
            float width = Screen.width;
            float space = (float)(width * 0.9f / ((_mapProperty.Row - 1) * 0.05f + _mapProperty.Row));
            float startX = 0;
            if (_mapProperty.Row % 2 == 0)
            {
                startX = (_mapProperty.Row / 2 - 0.5f + (_mapProperty.Row / 2 - 0.5f) * 0.05f) * space;
            }
            else
            {
                startX = (_mapProperty.Row / 2  + (_mapProperty.Row / 2) * 0.05f) * space;
            }

            squareMatrix = new SquareController[_mapProperty.Row, _mapProperty.Column];
            float startY = -1 * startX;
            float delta = space * 1.05f;
            float squareScale = space / 100f;
            float currentX = startX;
            float currentY = startY;
            //gen square controller
            for (int i=0; i<_mapProperty.Row; ++i)
            {
                for(int j=0;j<_mapProperty.Column; ++j)
                {
                    //TODO
                    SquareController newSquare = Instantiate(squarePrefab, Vector3.zero, Quaternion.identity, squareParent).GetComponent<SquareController>();
                    newSquare.transform.localPosition = new Vector3(currentX, currentY, 0);
                    newSquare.transform.localScale = squareScale * Vector3.one;
                    squareMatrix[i,j] = newSquare;
                    _mapProperty.Matrix[i, j] = (int)newSquare._cubeType;
                    currentX -= delta;
                }
                currentX = startX;
                currentY += delta;
            }

        }

        settingPanel.SetActive(false);
    }

    public void OnResetMap()
    {
        for (int i = 0; i < _mapProperty.Row; ++i)
        {
            for (int j = 0; j < _mapProperty.Column; ++j)
            {
                squareMatrix[i, j].SetType(CubeType.Ground);
                _mapProperty.Matrix[i, j] = (int)CubeType.Ground;
            }
        }
    }

    public void OnSaveMap()
    {
        for (int i = 0; i < _mapProperty.Row; ++i)
        {
            for (int j = 0; j < _mapProperty.Column; ++j)
            {
                _mapProperty.Matrix[i, j] = (int)squareMatrix[i, j]._cubeType;
            }
        }
        SaveFile(_mapProperty, "Assets/Resources/Maps/Map_" + ++_availableLevel + ".json");
    }

    private void SaveFile(MapProperty anObject, string path)
    {
        //Path eg: "Assets/Resources/test.json"
        var content = JsonConvert.SerializeObject(anObject);
        Debug.Log(content);
        var writer = new StreamWriter(path, false);
        writer.Write(content);
        writer.Close();
#if UNITY_EDITOR
        // in Unity need to refresh the data base
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    //Load the map from .json
    //public void Load()
    //{
    //    _newMapProperty = LoadFile("Maps/Map_1");
    //    GenerateMap(_newMapProperty);
    //    InitPlayer();
    //}

    public static int AvailableLevelCount()
    {
        DirectoryInfo mapDir = new DirectoryInfo("Assets/Resources/Maps");
        int i = 0;
        // Add file sizes.
        FileInfo[] fis = mapDir.GetFiles();
        foreach (FileInfo fi in fis)
        {
            if (fi.Extension.Contains("json"))
                i++;
        }
        return i;
    }
}
