using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public enum SwipeDirection
{
    Up,
    Down,
    Left,
    Right,
    Cancel
}
public class TouchScreenController : MonoBehaviour
{
    [SerializeField] private float dragThreshold;
    private SwipeDirection _swipeDirection;
    private bool _isDragging;
    private float _angle, _distance;

    private Vector2 _startPosition;

    private Vector2 _currentPosition;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _isDragging = true;
            _startPosition = Input.mousePosition;
            // Debug.Log("Mouse down");
        }
        else if(Input.GetMouseButtonUp(0))
        {
            _isDragging = false;
            // Debug.Log(("Mouse up"));
        }
        
        if (_isDragging && Input.GetMouseButton(0))
        {
            _currentPosition = Input.mousePosition;
            // Debug.Log("Mouse drag");
            _distance = Vector2.Distance(_currentPosition, _startPosition);
            if (_distance > dragThreshold)
            {
                _angle = Vector2.Angle(_currentPosition - _startPosition, Vector2.up);
                if (_angle < 45f || _angle > 135f)
                {
                    _swipeDirection = _currentPosition.y < _startPosition.y ? SwipeDirection.Down : SwipeDirection.Up;
                }
                else _swipeDirection = _currentPosition.x < _startPosition.x ? SwipeDirection.Left : SwipeDirection.Right;

                PlayerCubeController.Instance.OnMove(_swipeDirection);
                Debug.Log("Swipe " + _swipeDirection);
                _isDragging = false;
            }
            else
            {
                _swipeDirection = SwipeDirection.Cancel;
            }
        }
    }
}
