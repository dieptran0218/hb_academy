using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float fieldOfViewDelta;
    [SerializeField] private float YDelta;
    [SerializeField] private int stackToZoomThreshold;
    [SerializeField] private Vector3 localPositionPreset;
    private Camera _camera;
    private float _originalFieldOfView;
    private Vector3 _originalPosition;
    private int _zoomLevel;

    public Camera PlayerCamera => _camera;
    public Vector3 LocalPositionPreset => localPositionPreset;

    public static CameraController Instance;
    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _originalFieldOfView = _camera.fieldOfView;
        _originalPosition = transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }
    }

    public void AdjustCamera(int stackCount)
    {
        int newZoomLevel = stackCount / stackToZoomThreshold;
        if(_zoomLevel < newZoomLevel)
        {
            _zoomLevel = newZoomLevel;

            DOTween.Sequence()
                .Append(transform.DOLocalMoveY(_originalPosition.y + _zoomLevel * YDelta, 0.5f).SetEase(Ease.Linear))
                .Join(DOTween.To(() => _camera.fieldOfView, x => _camera.fieldOfView = x, _originalFieldOfView + _zoomLevel * fieldOfViewDelta, 0.5f).SetEase(Ease.Linear));
        }
    }

    public void ResetCamera()
    {
        transform.localPosition = LocalPositionPreset;
        _camera.fieldOfView = _originalFieldOfView;
    }
}
