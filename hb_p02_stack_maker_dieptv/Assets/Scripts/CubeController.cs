using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CubeType
{
    Ground,
    Stackable,
    Unstackable,
    Start,
    Finish,
    Default,
    InactiveStack
}
public class CubeController : MonoBehaviour
{
    [SerializeField] private CubeType cubeType;
    
    private Vector2 _cubeIndex;

    public Vector2 CubeIndex => _cubeIndex;

    public CubeType Type
    {
        get => cubeType;
        set => cubeType = value;
    }

    public void SetIndex(Vector2 index)
    {
        _cubeIndex = index;
    }
    
    public void SetIndex(int x, int y)
    {
        _cubeIndex = new Vector2(x,y);
    }
}
