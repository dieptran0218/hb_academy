using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public CubeController[,] CubeControllerMatrix;
    public MapProperty CurrentMapProperty;

    public static GameManager Instance;
    // Start is called before the first frame update
    private void Start()
    {
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }
        DontDestroyOnLoad(this);


        if(DataController.Instance.m_PlayerData.playingMap == null)
        {
            LevelManager.Instance.LoadLevel(DataController.Instance.m_PlayerData.level);
            //DataController.Instance.m_PlayerData.playingMap = new MapProperty();
            DataController.Instance.m_PlayerData.playingMap = CurrentMapProperty;
        }
        else
        {
            LevelManager.Instance.LoadOldMap();
        }

    }

    // [ContextMenu("Instantiate the map")]
    // public void CreateMap()
    // {
    //
    // }
    

    public List<CubeController> GetListCube(SwipeDirection inputDirection)
    {
        List<CubeController> cubeList = new List<CubeController>();
        switch (inputDirection)
        {
            case SwipeDirection.Up:
                //kiểm tra cube bên trên
                cubeList = CheckUpDirection();
                break;
            case SwipeDirection.Down:
                cubeList = CheckDownDirection();
                break;
            case SwipeDirection.Left:
                cubeList = CheckLeftDirection();
                break;
            case SwipeDirection.Right:
                cubeList = CheckRightDirection();
                break;
        }

        return cubeList;
    }

    private List<CubeController> CheckUpDirection()
    {
        bool isLoopBroken = false;
        int stackCount = PlayerCubeController.Instance.StackCount;
        List<CubeController> cubeList = new List<CubeController>();
        while (!isLoopBroken && PlayerCubeController.Instance.Index.x + 1 < CurrentMapProperty.Row && CubeControllerMatrix[(int)PlayerCubeController.Instance.Index.x + 1,(int)PlayerCubeController.Instance.Index.y].Type != CubeType.Ground)
        {
            CubeController nextCube = CubeControllerMatrix[(int)PlayerCubeController.Instance.Index.x + 1, (int)PlayerCubeController.Instance.Index.y];

            switch (nextCube.Type)
            {
                case CubeType.Stackable:
                    cubeList.Add(nextCube);
                    ++stackCount;
                    PlayerCubeController.Instance.Index += Vector2.right;
                    CurrentMapProperty.Matrix[(int)nextCube.CubeIndex.x,(int)nextCube.CubeIndex.y] = (int)CubeType.Default;
                    break;
                case CubeType.Unstackable:
                    if (stackCount > 0)
                    {
                        --stackCount;
                        cubeList.Add(nextCube);
                        PlayerCubeController.Instance.Index += Vector2.right;
                        CurrentMapProperty.Matrix[(int)nextCube.CubeIndex.x, (int)nextCube.CubeIndex.y] = (int)CubeType.InactiveStack;
                    }
                    else isLoopBroken = true;
                    break;
                case CubeType.Default:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.right;
                    continue;
                case CubeType.InactiveStack:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.right;
                    continue;
                case CubeType.Start:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.right;
                    continue;
                case CubeType.Finish:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.right;
                    continue;
            }
        }
        return cubeList; 
    }
    
    private List<CubeController> CheckDownDirection()
    {
        bool isLoopBroken = false;
        int stackCount = PlayerCubeController.Instance.StackCount;
        List<CubeController> cubeList = new List<CubeController>();
        while (!isLoopBroken && PlayerCubeController.Instance.Index.x - 1 > -1 && CubeControllerMatrix[(int)PlayerCubeController.Instance.Index.x - 1,(int)PlayerCubeController.Instance.Index.y].Type != CubeType.Ground)
        {
            CubeController nextCube = CubeControllerMatrix[(int)PlayerCubeController.Instance.Index.x - 1, (int)PlayerCubeController.Instance.Index.y];
            switch (nextCube.Type)
            {
                case CubeType.Stackable:
                    cubeList.Add(nextCube);
                    ++stackCount;
                    PlayerCubeController.Instance.Index += Vector2.left;
                    CurrentMapProperty.Matrix[(int)nextCube.CubeIndex.x, (int)nextCube.CubeIndex.y] = (int)CubeType.Default;
                    break;
                case CubeType.Unstackable:
                    if (stackCount > 0)
                    {
                        --stackCount;
                        cubeList.Add(nextCube);
                        PlayerCubeController.Instance.Index += Vector2.left;
                        CurrentMapProperty.Matrix[(int)nextCube.CubeIndex.x, (int)nextCube.CubeIndex.y] = (int)CubeType.InactiveStack;
                    }
                    else isLoopBroken = true;
                    break;
                case CubeType.Default:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.left;
                    continue;
                case CubeType.InactiveStack:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.left;
                    continue;
                case CubeType.Start:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.left;
                    continue;
                case CubeType.Finish:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.left;
                    continue;
            }
        }
        return cubeList; 
    }
    
    private List<CubeController> CheckRightDirection()
    {
        bool isLoopBroken = false;
        int stackCount = PlayerCubeController.Instance.StackCount;
        List<CubeController> cubeList = new List<CubeController>();
        while (!isLoopBroken && PlayerCubeController.Instance.Index.y - 1 > -1 && CubeControllerMatrix[(int)PlayerCubeController.Instance.Index.x,(int)PlayerCubeController.Instance.Index.y - 1].Type != CubeType.Ground)
        {
            CubeController nextCube = CubeControllerMatrix[(int)PlayerCubeController.Instance.Index.x, (int)PlayerCubeController.Instance.Index.y - 1];

            switch (nextCube.Type)
            {
                case CubeType.Stackable:
                    cubeList.Add(nextCube);
                    ++stackCount;
                    PlayerCubeController.Instance.Index += Vector2.down;
                    CurrentMapProperty.Matrix[(int)nextCube.CubeIndex.x, (int)nextCube.CubeIndex.y] = (int)CubeType.Default;
                    break;
                case CubeType.Unstackable:
                    if (stackCount > 0)
                    {
                        --stackCount;
                        cubeList.Add(nextCube);
                        PlayerCubeController.Instance.Index += Vector2.down;
                        CurrentMapProperty.Matrix[(int)nextCube.CubeIndex.x, (int)nextCube.CubeIndex.y] = (int)CubeType.InactiveStack;
                    }
                    else isLoopBroken = true;
                    break;
                case CubeType.InactiveStack:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.down;
                    continue;
                case CubeType.Default:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.down;
                    continue;
                case CubeType.Start:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.down;
                    continue;
                case CubeType.Finish:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.down;
                    continue;
            }
        }
        return cubeList; 
    }
    
    private List<CubeController> CheckLeftDirection()
    {
        bool isLoopBroken = false;
        int stackCount = PlayerCubeController.Instance.StackCount;
        List<CubeController> cubeList = new List<CubeController>();
        while (!isLoopBroken && PlayerCubeController.Instance.Index.y + 1 < CurrentMapProperty.Column && CubeControllerMatrix[(int)PlayerCubeController.Instance.Index.x,(int)PlayerCubeController.Instance.Index.y + 1].Type != CubeType.Ground)
        {
            CubeController nextCube = CubeControllerMatrix[(int)PlayerCubeController.Instance.Index.x, (int)PlayerCubeController.Instance.Index.y + 1];
            switch (nextCube.Type)
            {
                case CubeType.Stackable:
                    cubeList.Add(nextCube);
                    ++stackCount;
                    PlayerCubeController.Instance.Index += Vector2.up;
                    CurrentMapProperty.Matrix[(int)nextCube.CubeIndex.x, (int)nextCube.CubeIndex.y] = (int)CubeType.Default;
                    break;
                case CubeType.Unstackable:
                    if (stackCount > 0)
                    {
                        --stackCount;
                        cubeList.Add(nextCube);
                        PlayerCubeController.Instance.Index += Vector2.up;
                        CurrentMapProperty.Matrix[(int)nextCube.CubeIndex.x, (int)nextCube.CubeIndex.y] = (int)CubeType.InactiveStack;
                    }
                    else isLoopBroken = true;
                    break;
                case CubeType.Default:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.up;
                    continue;
                case CubeType.InactiveStack:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.up;
                    continue;
                case CubeType.Start:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.up;
                    continue;
                case CubeType.Finish:
                    cubeList.Add(nextCube);
                    PlayerCubeController.Instance.Index += Vector2.up;
                    continue;
            }
        }
        return cubeList; 
    }

    public void OnGameWin()
    {
        Debug.Log("Victory");
        UpdatePlayerData();
        PlayerCubeController.Instance.DownStackPlayer(0, ShowGameWin);
    }

    public void ShowGameWin(bool isSuccess)
    {
        UIManager.Instance.setCoinText(DataController.Instance.m_PlayerData.coin);
        UIManager.Instance.DisplayWinPanel(true);
    }

    private void UpdatePlayerData(bool isSuccess = true)
    {
        DataController.Instance.m_PlayerData.level += 1;
        DataController.Instance.m_PlayerData.coin += PlayerCubeController.Instance.StackCount;
        DataController.Instance.m_PlayerData.playingMap = null;
        DataController.Instance.m_PlayerData.stackCount = 0;
        DataController.Instance.SavePlayerData();
    }
}