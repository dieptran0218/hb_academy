using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PlayerData
{
    public bool music = true, sfx = true;
    public int level = 1;
    public int coin = 0;
    public int stackCount = 0;
    public int indexX = 0, indexY = 0;
    public MapProperty playingMap = null;
}

public class DataController : MonoBehaviour
{
    public PlayerData m_PlayerData;
    public static DataController Instance;
    public StreamWriter writer;
    private void Start()
    {
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }
        DontDestroyOnLoad(this);
        StartCoroutine(LoadPlayerData(OnGetData));
    }

    public void SavePlayerData()
    {
        if(m_PlayerData.level == 0)
        {
            m_PlayerData.level = 1;
        }
        var content = JsonConvert.SerializeObject(m_PlayerData);
        writer = new StreamWriter("Assets/Resources/PlayerData.json", false);
        Debug.Log(content);
        writer.Write(content);
        writer.Flush();
        writer.Close();
#if UNITY_EDITOR
        // in Unity need to refresh the data base
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    IEnumerator LoadPlayerData(System.Action<bool> callback)
    {
        var jsonTextFile = Resources.Load<TextAsset>("PlayerData");
        m_PlayerData = JsonConvert.DeserializeObject<PlayerData>(jsonTextFile.text); 
        yield return new WaitForSeconds(3f);
        if (m_PlayerData == null)
        {
            Debug.Log("PlayerData is null, initiating new profile");
            m_PlayerData = new PlayerData();
            UIManager.Instance.setCoinText(m_PlayerData.coin);
            SavePlayerData();
        }

        callback(true);
    }

    [ContextMenu("Reset PlayerData")]
    public void Save()
    {
        var content = "";
        writer = new StreamWriter("PlayerData.json", false);
        Debug.Log(content);
        writer.Write(content);
        writer.Flush();
        writer.Close();

#if UNITY_EDITOR
        // in Unity need to refresh the data base
        UnityEditor.AssetDatabase.Refresh();
#endif
    }

    public void OnGetData(bool isSuccess)
    {
        if (isSuccess)
        {
            UIManager.Instance.LoadGameScene();
        }
        else
        {
            Debug.LogError("Data not avaiable");
        }
    }

}
