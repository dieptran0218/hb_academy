using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI loadingText;
    [SerializeField] private TextMeshProUGUI coinText;
    [SerializeField] private TextMeshProUGUI levelText;

    [SerializeField] private TextMeshProUGUI winPanelCoinText;
    [SerializeField] private GameObject winPanel;
    [SerializeField] private Image coverImage;
    [SerializeField] private GameObject loadingObjects;
    [SerializeField] private GameObject gameplayObjects;
    private bool _isLoading = true;
    public static UIManager Instance;
    // Start is called before the first frame update
    void Start()
    {
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }
        DontDestroyOnLoad(this);
        gameplayObjects.transform.localScale = Vector3.zero;
        gameplayObjects.SetActive(false);
        winPanel.SetActive(false);
    }

    private void OnEnable()
    {
        if (SceneManager.GetActiveScene().name.Equals("DataLoad"))
        {
            StartCoroutine(LoadingTextLoop());
        }
    }

    IEnumerator LoadingTextLoop()
    {
        while (_isLoading)
        {
            loadingText.text = "Loading.";
            yield return new WaitForSeconds(0.5f);
            loadingText.text = "Loading..";
            yield return new WaitForSeconds(0.5f);
            loadingText.text = "Loading...";
            yield return new WaitForSeconds(0.5f);
        }
        yield return 0;
    }

    public void LoadGameScene()
    {
        DOTween.Sequence()
            .Append(coverImage.DOFade(1f, 0.75f).SetEase(Ease.Linear))
            .AppendCallback(() =>
            {
                loadingObjects.SetActive(false);
                SceneManager.LoadScene(1);
                gameplayObjects.SetActive(true);
            })
            .Append(coverImage.DOFade(0f, 0.75f))
            .Join(gameplayObjects.transform.DOScale(Vector3.one,0.75f));
    }

    public void PlayNewLevelTransistion(int level)
    {
        DOTween.Sequence()
        .Append(coverImage.DOFade(1f, 0.75f).SetEase(Ease.Linear))
        .AppendCallback(() =>
        {
            LevelManager.Instance.StartNewLevel(level);
        })
        .Append(coverImage.DOFade(0f, 0.75f));
    }

    public void setCoinText(int coin)
    {
        coinText.text = coin + "";
        winPanelCoinText.text = coin + "";
    }

    public void setLevel(int level)
    {
        levelText.text = "Level: " + level;
    }

    public void OnNextLevelClicked()
    {
        DisplayWinPanel(false);
        LevelManager.Instance.LoadLevel(DataController.Instance.m_PlayerData.level);
    }

    public void OnNextLevelAborted()
    {
        DisplayWinPanel(false);
        Application.Quit();
    }

    public void DisplayWinPanel(bool isActive)
    {
        winPanel.SetActive(isActive);
    }
}
