using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum PlayerState
{
    Moving,
    Idle
}
public class PlayerCubeController : MonoBehaviour
{
    [SerializeField] private float travelTime; //time to get to next cube
    [SerializeField] private GameObject visualCube;
    [SerializeField] private Transform visualGroup;

    private PlayerState _playerState;
    private Stack<GameObject> _visualCubeStack;
    private int _stackCount;

    public int StackCount => _stackCount;
    public Vector2 Index { get; set; }
    public PlayerState CurrentState => _playerState;
    public static PlayerCubeController Instance;

    private void Awake()
    {
        _visualCubeStack = new Stack<GameObject>();
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        Init();
    }

    public void Init()
    {
        _playerState = PlayerState.Idle;
        CameraController.Instance.transform.parent = transform.parent;
        CameraController.Instance.transform.localPosition = CameraController.Instance.LocalPositionPreset;
        visualGroup.localPosition += Vector3.down * visualGroup.localPosition.y;
        UpStackPlayer(DataController.Instance.m_PlayerData.stackCount);
        CameraController.Instance.ResetCamera();
    }

    public void Init(Vector3 startPosition)
    {
        _playerState = PlayerState.Idle;
        CameraController.Instance.transform.parent = transform.parent;
        CameraController.Instance.transform.localPosition = CameraController.Instance.LocalPositionPreset;
        visualGroup.localPosition += Vector3.down * visualGroup.localPosition.y;
        UpStackPlayer(DataController.Instance.m_PlayerData.stackCount);
        CameraController.Instance.ResetCamera();
        transform.parent.position = startPosition;
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    public void OnMove(SwipeDirection inputDirection)
    {
        if (_playerState == PlayerState.Moving) return;

        List<CubeController> listCube = GameManager.Instance.GetListCube(inputDirection);
        if(listCube.Count == 0)
        {
            Debug.LogError("No Cube on direction: " + inputDirection);
            return;
        };
        int i = 0;
        _playerState = PlayerState.Moving;
        DOTween.Sequence()
            .Append(transform.parent.DOMove(new Vector3(listCube[i].transform.position.x, transform.parent.position.y, listCube[i].transform.position.z), travelTime).SetEase(Ease.Linear))
            .AppendCallback(() =>
            {
                switch (listCube[i].Type)
                {
                    case CubeType.Stackable:
                        ++_stackCount;
                        listCube[i].Type = CubeType.Default;
                        listCube[i].transform.GetChild(0).gameObject.SetActive(false);
                        UpStackPlayer();
                        break;
                    case CubeType.Unstackable:
                        --_stackCount;
                        listCube[i].Type = CubeType.InactiveStack;
                        Instantiate(visualCube, listCube[i].transform.position, Quaternion.identity,
                            listCube[i].transform.parent);
                        DownStackPlayer();
                        break;
                    case CubeType.Finish:
                        GameManager.Instance.OnGameWin();
                        break;
                }
                ++i;
            })
            .SetLoops(listCube.Count, LoopType.Incremental)
            .OnComplete(()=> {
                _playerState = PlayerState.Idle;

                //UpdatePlayerData
                if (listCube.Count > 0)
                {
                    DataController.Instance.m_PlayerData.stackCount = _stackCount;
                    DataController.Instance.m_PlayerData.playingMap = GameManager.Instance.CurrentMapProperty;
                    DataController.Instance.m_PlayerData.indexX = (int)listCube[listCube.Count - 1].CubeIndex.x;
                    DataController.Instance.m_PlayerData.indexY = (int)listCube[listCube.Count - 1].CubeIndex.y;
                    DataController.Instance.SavePlayerData();
                }
            });

    }

    private void UpStackPlayer()
    {
        _visualCubeStack.Push(Instantiate(visualCube, transform.position, Quaternion.identity, visualGroup));
        visualGroup.DOLocalMoveY(visualGroup.localPosition.y + 0.3f, travelTime * 0.75f).SetEase(Ease.Linear);
        CameraController.Instance.AdjustCamera(_stackCount);
    }

    public void UpStackPlayer(int level)
    {
        _stackCount = level;
        for(int i = 1; i < level; ++i)
        {
            _visualCubeStack.Push(Instantiate(visualCube, transform.position, Quaternion.identity, visualGroup));
            visualGroup.localPosition += Vector3.up * 0.3f;
        }

        CameraController.Instance.AdjustCamera(level);
    }

    private void DownStackPlayer()
    {
        visualGroup.DOLocalMoveY(visualGroup.localPosition.y - 0.3f, travelTime * 0.75f).SetEase(Ease.Linear).OnComplete(()=> {
            Destroy(_visualCubeStack.Pop());
        });
        CameraController.Instance.AdjustCamera(_stackCount);
    }

    public void DownStackPlayer(int level, System.Action<bool> callback)
    {
        DOTween.Sequence()
            .Append(visualGroup.DOLocalMoveY(visualGroup.localPosition.y - 0.3f, travelTime * 0.25f).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    if(_visualCubeStack.Count > level)
                    {
                        Destroy(_visualCubeStack.Pop());
                    }
                }))
            .AppendCallback(()=> {
                --_stackCount;
                CameraController.Instance.AdjustCamera(_stackCount);
                if(_stackCount == level || visualGroup.localPosition.y < 0.1f)
                {
                    DOTween.KillAll();
                    callback(true);
                }
            })
            .SetLoops(-1,LoopType.Incremental);
    }
}
