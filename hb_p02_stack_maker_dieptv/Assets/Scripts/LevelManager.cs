﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;
public class MapProperty
{
    public int Column = 0, Row = 0;
    public int[,] Matrix = new int[0,0];
}
public class LevelManager : MonoBehaviour
{
    [SerializeField] private Transform mapParent;
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private GameObject stackablePrefab;
    [SerializeField] private GameObject unstackablePrefab;
    [SerializeField] private GameObject groundPrefab;
    [SerializeField] private GameObject startPrefab;
    [SerializeField] private GameObject finishPrefab;
    [SerializeField] private GameObject defaultPrefab;
    [SerializeField] private GameObject inactiveStackPrefab;
    private MapProperty _newMapProperty;
    private CubeController _startCube;
    private List<GameObject> listBoundary;


    public static LevelManager Instance;

    private void Start()
    {
        if (Instance == null || Instance != this)
        {
            Instance = this;
        }
        DontDestroyOnLoad(this);
        DontDestroyOnLoad(mapParent);
    }

    // [ContextMenu("Instantiate the map")]
    // public void CreateMap()
    // {
    //
    // }

    public void LoadLevel(int level)
    {
        if(level > AvailableLevelCount())
        {
            Debug.LogError("The level " + level + " is not available. Please try again later!");
            Application.Quit();
            return;
        }

        if(level > 1)
        {
            UIManager.Instance.PlayNewLevelTransistion(level);
        }
        else
        {
            StartNewLevel(1);
        }
    }

    public void StartNewLevel(int level)
    {
        ClearMap();
        _newMapProperty = LoadFile("Maps/Map_" + level);
        GenerateMap(_newMapProperty);
        DataController.Instance.m_PlayerData.level = level;
        DataController.Instance.m_PlayerData.stackCount = 0;
        DataController.Instance.m_PlayerData.indexX = (int)_startCube.CubeIndex.x;
        DataController.Instance.m_PlayerData.indexY = (int)_startCube.CubeIndex.y;
        InitPlayer();
        UIManager.Instance.setLevel(level);
    }

    private void ClearMap()
    {
        for (int i = mapParent.childCount - 1; i != -1; --i)
        {
            Destroy(mapParent.GetChild(i).gameObject);
        }
    }

    public void LoadOldMap()
    {
        UIManager.Instance.setLevel(DataController.Instance.m_PlayerData.level);
        UIManager.Instance.setCoinText(DataController.Instance.m_PlayerData.coin);
        GenerateMap(DataController.Instance.m_PlayerData.playingMap);
        InitOldPlayer();
    }


    private void SaveFile(MapProperty anObject, string path)
    {
        //Path eg: "Assets/Resources/test.json"
        var content = JsonConvert.SerializeObject(anObject);
        Debug.Log(content);
        var writer = new StreamWriter(path, false);
        writer.Write(content);
        writer.Close();
    }

    private MapProperty LoadFile(string path)
    {
        var jsonTextFile = Resources.Load<TextAsset>(path);
        return JsonConvert.DeserializeObject<MapProperty>(jsonTextFile.text);
    }

    private void GenerateMap(MapProperty mapProperty)
    {
        if (mapProperty == null)
        {
            Debug.LogError("Map is null - not deserialized properly");
            return;
        }

        GameManager.Instance.CurrentMapProperty = mapProperty;
        GameManager.Instance.CubeControllerMatrix = new CubeController[mapProperty.Row, mapProperty.Column];
        for (var i = 0; i < mapProperty.Row; ++i)
            for (var j = 0; j < mapProperty.Column; ++j)
            {
                CubeController newCube = null;
                switch (mapProperty.Matrix[i, j])
                {
                    case (int) CubeType.Ground:
                        newCube = Instantiate(groundPrefab, new Vector3(i + 1, 0, j + 1), Quaternion.identity,
                            mapParent).GetComponent<CubeController>();
                        break;
                    case (int)CubeType.Stackable:
                        newCube = Instantiate(stackablePrefab, new Vector3(i + 1, 0, j + 1), Quaternion.identity,
                            mapParent).GetComponent<CubeController>();
                        break;
                    case (int)CubeType.Unstackable:
                        newCube = Instantiate(unstackablePrefab, new Vector3(i + 1, -0.3f, j + 1), Quaternion.identity,
                            mapParent).GetComponent<CubeController>();
                        break;
                    case (int)CubeType.Start:
                        newCube = Instantiate(startPrefab, new Vector3(i + 1, 0, j + 1), Quaternion.identity,
                            mapParent).GetComponent<CubeController>();
                        _startCube = newCube;
                        break;
                    case (int)CubeType.Finish:
                        newCube = Instantiate(finishPrefab, new Vector3(i + 1, 0, j + 1), Quaternion.identity,
                            mapParent).GetComponent<CubeController>();
                        break;
                    case (int)CubeType.Default:
                        newCube = Instantiate(defaultPrefab, new Vector3(i + 1, 0, j + 1), Quaternion.identity,
                            mapParent).GetComponent<CubeController>();
                        break;
                    case (int) CubeType.InactiveStack:
                        newCube = Instantiate(inactiveStackPrefab, new Vector3(i + 1, 0, j + 1), Quaternion.identity,
                            mapParent).GetComponent<CubeController>();
                        break;
                }

                if (newCube != null)
                {
                    newCube.SetIndex(i,j);
                    GameManager.Instance.CubeControllerMatrix[i,j] = newCube;
                }
            }

        //Tạo viền bao quanh map
        if(listBoundary == null)
        {
            listBoundary = new List<GameObject>();
        }
        else
        {
            while (listBoundary.Count > 0)
            {
                Destroy(listBoundary[listBoundary.Count - 1]);
                listBoundary.RemoveAt(listBoundary.Count - 1);
            }
        }

        for (int i = -1; i < mapProperty.Row + 1; ++i)
        {
            listBoundary.Add(Instantiate(groundPrefab, new Vector3(i + 1, 0, mapProperty.Column + 1), Quaternion.identity,
                        mapParent));
            listBoundary.Add(Instantiate(groundPrefab, new Vector3(i + 1, 0 , 0), Quaternion.identity,
                        mapParent));
        }

        for (int i = -1; i < mapProperty.Column + 1; ++i)
        {
            listBoundary.Add(Instantiate(groundPrefab, new Vector3(mapProperty.Row + 1, 0, i + 1), Quaternion.identity,
                        mapParent));
            listBoundary.Add(Instantiate(groundPrefab, new Vector3(0, 0, i + 1), Quaternion.identity,
            mapParent));
        }
    }

    private void InitPlayer()
    {
        Vector3 startPosition = _startCube.transform.position;
        if(PlayerCubeController.Instance == null)
        {
            PlayerCubeController.Instance = Instantiate(playerPrefab,
                new Vector3(startPosition.x, playerPrefab.transform.position.y, startPosition.z),
                Quaternion.identity, null).transform.GetChild(0).GetComponent<PlayerCubeController>();
        }
        else
        {
            PlayerCubeController.Instance.Init(startPosition);
        }

        PlayerCubeController.Instance.Index = _startCube.CubeIndex;

    }

    private void InitOldPlayer()
    {
        Vector3 position = GameManager.Instance.CubeControllerMatrix[(int)DataController.Instance.m_PlayerData.indexX, (int)DataController.Instance.m_PlayerData.indexY].transform.position;
        PlayerCubeController.Instance = Instantiate(playerPrefab,
            new Vector3(position.x, playerPrefab.transform.position.y, position.z),
            Quaternion.identity, null).transform.GetChild(0).GetComponent<PlayerCubeController>();
        PlayerCubeController.Instance.Index = new Vector2(DataController.Instance.m_PlayerData.indexX, DataController.Instance.m_PlayerData.indexY);
    }

    public static int AvailableLevelCount()
    {
        DirectoryInfo mapDir = new DirectoryInfo("Assets/Resources/Maps");
        int i = 0;
        // Add file sizes.
        FileInfo[] fis = mapDir.GetFiles();
        foreach (FileInfo fi in fis)
        {
            if (fi.Extension.Contains("json"))
                i++;
        }
        return i;
    }
}
