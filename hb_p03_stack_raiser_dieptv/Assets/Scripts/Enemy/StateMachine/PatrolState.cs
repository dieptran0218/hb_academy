using UnityEngine;

public class PatrolState : IState
{
    public void OnEnter(EnemyController enemy)
    {
        enemy.StackToLay = Random.Range(3, 5);
        enemy.FindAndGoToNextTarget();
    }

    public void OnExecute(EnemyController enemy)
    {
    }

    public void OnExit(EnemyController enemy)
    {

    }
}
