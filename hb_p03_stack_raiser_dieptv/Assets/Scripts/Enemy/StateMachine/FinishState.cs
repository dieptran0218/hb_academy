﻿using UnityEngine;

public class FinishState : IState
{
    public void OnEnter(EnemyController enemy)
    {
        BridgeController bestBridge = enemy.FindBestBridge();
        enemy.GoTo(bestBridge.EndPoint.position);
    }

    public void OnExecute(EnemyController enemy)
    {

    }

    public void OnExit(EnemyController enemy)
    {

    }
}
