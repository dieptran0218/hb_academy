using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : Character
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float stopThreshold;
    private Rigidbody rb;
    private int _stackToLay = 0;
    private NavMeshAgent _navMeshAgent;
    private int _floorIndex = 0;

    private IState currentState;
    private bool isFindingStack = false;
    private Vector2 _lastPickUpStackIndex;

    public Vector2 LastPickUpStackIndex
    {
        get => _lastPickUpStackIndex;
        set => _lastPickUpStackIndex = value;
    }

    public int StackToLay
    {
        get => _stackToLay;
        set => _stackToLay = value;
    }

    protected override void Awake()
    {
        base.Awake();
        _lastPickUpStackIndex = -1 * Vector2.one;
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.speed = moveSpeed;
        rb = GetComponent<Rigidbody>();
        _floorIndex = 0;
    }

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        Init();
    }
    private void Update()
    {
        if (currentState != null)
        {
            currentState.OnExecute(this);
        }

        if(_navMeshAgent != null && _navMeshAgent.remainingDistance < stopThreshold)
        {
            StopMoving(true);
        }
    }

    private void Init()
    {
        ChangeState(new IdleState());
    }

    public void ChangeState(IState newState)
    {
        if (currentState != null)
        {
            currentState.OnExit(this);
        }

        currentState = newState;
        //Debug.Log(newState.ToString());
        if (currentState != null)
        {
            currentState.OnEnter(this);
        }
    }

    public void StopMoving(bool setIdle)
    {
        if (setIdle)
        {
            ChangeAnim("Idle");
            //TODO
        }
        _navMeshAgent.isStopped = true;
    }


    public void FindAndGoToNextTarget()
    {
        isFindingStack = true;
        GoTo(GameManager.Instance.FloorList[_floorIndex].GetRandomTarget(this));
    }

    protected override void GetStack(StackController newStack)
    {
        base.GetStack(newStack);
        if (isFindingStack)
        {
            //StackController = other.GetComponent<StackController>();
            isFindingStack = false;
            --_stackToLay;
            if (StackToLay > 0)
            {
                FindAndGoToNextTarget();
            }
            else
            {
                ChangeState(new FinishState());
            }
        }
    }

    internal BridgeController FindBestBridge()
    {
        int bestBridgeIndex = UnityEngine.Random.Range(0, GameManager.Instance.FloorList[_floorIndex].BridgeList.Count);
        int bestScore = GameManager.Instance.FloorList[_floorIndex].BridgeList[0].Evaluate(StackQueue.Count, stackType);
        for (int i = GameManager.Instance.FloorList[_floorIndex].BridgeList.Count - 1; i != -1; --i)
        {
            if (i == bestBridgeIndex) continue;

            int score = GameManager.Instance.FloorList[_floorIndex].BridgeList[i].Evaluate(StackQueue.Count, stackType);
            if (score > bestScore)
            {
                bestBridgeIndex = i;
                bestScore = score;
            }
        }
        return GameManager.Instance.FloorList[_floorIndex].BridgeList[bestBridgeIndex];
    }

    public override void OnRunOutOfStack()
    {
        base.OnRunOutOfStack();
        StopMoving(false);
        ChangeState(new PatrolState());
    }

    public override void OnUnlockBridge()
    {
        base.OnUnlockBridge();

        if (++_floorIndex < GameManager.Instance.FloorList.Count)
        {
            ChangeState(new PatrolState());
        }
        else
        {
            ChangeState(new VictoryState());
        }

    }

    public void GoTo(Vector3 position)
    {
        _navMeshAgent.SetDestination(position);
        _navMeshAgent.isStopped = false;
        ChangeAnim("Run");
    }

    public override void FailStopMoving()
    {
        base.FailStopMoving();
        if (_navMeshAgent != null)
        {
            _navMeshAgent.SetDestination(transform.position);
            _navMeshAgent.speed = 0;
            _navMeshAgent.isStopped = true;
        }
        GetComponent<Collider>().enabled = false;
        ChangeState(new VictoryState());
        ChangeAnim("Idle");
    }

    public override void OnVictory(Transform centerPoint)
    {
        ChangeState(new VictoryState());
        _navMeshAgent.speed = 0;
        _navMeshAgent.isStopped = true;
        base.OnVictory(centerPoint);
    }
}
