﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Note: kéo thả stacktype và stackPrefabs trong StackGenerator theo thứ tự này 
public enum StackType
{
    Red,
    Green,
    Blue,
    Yellow
}

public enum StackState
{
    OnFloor,
    OnBridge,
    OnStack
}

public class StackController : MonoBehaviour
{
    [SerializeField] private StackType stackType;
    [SerializeField] private Material redMaterial, greenMaterial, blueMaterial, yellowMaterial;
    private StackState _stackState;
    private Vector2 _index;
    private MeshRenderer _meshRenderer;
    private StackGenerator _stackGenerator;
    private BridgeController _parentBridge;
    private int _indexInBridgeList;

    public int IndexInBridgeList
    {
        get => _indexInBridgeList;
        set => _indexInBridgeList = value;
    }

    public BridgeController ParentBridge
    {
        get => _parentBridge;
        set => _parentBridge = value;
    }

    public StackGenerator m_StackGenerator {
        get => _stackGenerator;
        set => _stackGenerator = value;
    }


    private void Awake()
    {
        _indexInBridgeList = -1;
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    public StackState StackState
    {
        get => _stackState;
        set => _stackState = value;
    }
    public StackType StackType
    {
        get => stackType;
        set => stackType = value;
    }
    public Vector2 Index
    {
        get => _index;
        set => _index = value;
    }

    private void Start()
    {
        _stackState = StackState.OnFloor;
    }

    public void OnCharacterReplaced(StackController newStack)
    {
        //Destroy(gameObject);
        //Debug.Log("Start respawning stack at index " + _index.x + " - " + _index.y);
        SetType(newStack.StackType);
        Destroy(newStack.gameObject);
    }

    public void OnCharacterTaken()
    {
        //Destroy(gameObject);
        //Debug.Log("Start respawning stack at index " + _index.x + " - " + _index.y);
        _stackState = StackState.OnStack;
        m_StackGenerator.RespawnDelay(_index);
    }

    public void SetType(StackType newType)
    {
        stackType = newType;
        switch (newType)
        {
            case StackType.Red:
                _meshRenderer.material = redMaterial;
                break;
            case StackType.Green:
                _meshRenderer.material = greenMaterial;
                break;
            case StackType.Blue:
                _meshRenderer.material = blueMaterial;
                break;
            case StackType.Yellow:
                _meshRenderer.material = yellowMaterial;
                break;
        }
    }

    public void OnLayOnBridge()
    {
        _stackState = StackState.OnBridge;
    }
}
