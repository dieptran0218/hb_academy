using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    [SerializeField] protected StackType stackType;
    [SerializeField] private Transform stackHolder;
    [SerializeField] private Transform newStackPosition;
    [SerializeField] private Material redMaterial, greenMaterial, blueMaterial, yellowMaterial;
    [SerializeField] private SkinnedMeshRenderer meshRenderer;
    [SerializeField] private Animator anim;
    private Queue<StackController> _stackQueue;
    private string currentAnim = "";

    public Queue<StackController> StackQueue => _stackQueue;
    public StackType m_StackType => stackType;

    protected virtual void Awake()
    {
        _stackQueue = new Queue<StackController>();
    }

    protected virtual void Start()
    {
        InitColor();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Stack"))
        {
            StackController newStack = other.GetComponent<StackController>();
            if (newStack == null) return;
            if (newStack.StackType == stackType)
            {
                //Debug.Log("got same stack type on the floor");
                if(newStack.StackState == StackState.OnFloor)
                {
                    GetStack(newStack);
                }
            }
            else if(newStack.StackType != stackType)
            {
                if(newStack.StackState == StackState.OnBridge)
                {
                    //Debug.Log("touch other stack on bridge");
                    if(_stackQueue.Count <= 0)
                    {
                        OnRunOutOfStack();
                    }
                    else
                    {
                        ReplaceStack(newStack);
                    }
                }
            }
        }
    }

    public virtual void OnUnlockBridge()
    {
        //OVERRIDE ON CHILD SCRIPTS
    }

    public virtual void OnRunOutOfStack()
    {
        //OVERRIDE ON CHILD SCRIPTS
    }

    protected virtual void GetStack(StackController newStack)
    {
        _stackQueue.Enqueue(newStack);
        newStack.transform.parent = stackHolder;
        DOTween.Sequence()
            .Append(newStack.transform.DOLocalMove(newStackPosition.localPosition, 0.25f))
            .Join(newStack.transform.DOLocalRotateQuaternion(Quaternion.identity, 0.25f))
            .Append(newStackPosition.DOLocalMoveY(newStackPosition.localPosition.y + 0.27f, 0f));

        newStack.OnCharacterTaken();
    }

    public void LayStack(BridgeController bridge)
    {
        StackController laidStack = _stackQueue.Dequeue();
        laidStack.OnLayOnBridge();
        bridge.LayStack(laidStack);
        stackHolder.DOLocalMoveZ(stackHolder.localPosition.z - 0.25f, 0.25f);
    }

    private void ReplaceStack(StackController replacedStack)
    {
        replacedStack.StackType = stackType;
        StackController newStack = _stackQueue.Dequeue();
        newStack.transform.parent = stackHolder;
        DOTween.Sequence()
            .Append(newStack.transform.DOMove(replacedStack.transform.position, 0.25f))
            .Join(newStack.transform.DORotateQuaternion(replacedStack.transform.rotation, 0.25f))
            .Join(newStack.transform.DOScale(newStack.transform.localScale, 0.15f))
            .Join(stackHolder.DOLocalMoveZ(stackHolder.localPosition.z - 0.25f, 0.25f))
            .AppendCallback(()=> { 
                if (replacedStack.IndexInBridgeList == replacedStack.ParentBridge.MaxStackCount - 1)
                {
                    OnUnlockBridge();
                }
                replacedStack.OnCharacterReplaced(newStack);
            });
    }

    protected void InitColor()
    {
        switch (stackType)
        {
            case StackType.Red:
                meshRenderer.material = redMaterial;
                break;
            case StackType.Green:
                meshRenderer.material = greenMaterial;
                break;
            case StackType.Blue:
                meshRenderer.material = blueMaterial;
                break;
            case StackType.Yellow:
                meshRenderer.material = yellowMaterial;
                break;
        }
    }

    public virtual void FailStopMoving()
    {

    }

    public virtual void OnVictory(Transform centerPoint)
    {
        CameraFollow.Instance.OnVictory(transform);
        ChangeAnim("Run");
        DOTween.Sequence()
            .Append(transform.DOMoveX(centerPoint.position.x, 0.3f).SetEase(Ease.Linear))
            .Join(transform.DOMoveZ(centerPoint.position.z, 0.3f).SetEase(Ease.Linear))
            .AppendCallback(()=> {
                ChangeAnim("Dance");
            })
            .Append(transform.DORotate(new Vector3(0, 180, 0), 1f).SetEase(Ease.Linear));
    }

    protected void ChangeAnim(string animName)
    {
        if (!anim.GetCurrentAnimatorClipInfo(0)[0].clip.name.Equals(animName))
        {
            currentAnim = animName;
            anim.ResetTrigger(animName);
            anim.SetTrigger(animName);
        }
    }
}
