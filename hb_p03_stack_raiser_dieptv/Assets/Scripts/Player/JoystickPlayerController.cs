﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerController : MonoBehaviour
{
    [SerializeField] private float moveSpeed, rotateSpeed;
    [SerializeField] private VariableJoystick variableJoystick;
    [SerializeField] private Rigidbody rb;
    [SerializeField] private float moveThreshold;

    private PlayerController _player;

    private void Awake()
    {
        _player = GetComponent<PlayerController>();
    }

    public void FixedUpdate()
    {
        if (!_player.CanMove) return;
        Vector3 direction = Vector3.forward * variableJoystick.Vertical + Vector3.right * variableJoystick.Horizontal;
        //rb.velocity = direction * speed;
        //rb.rotation = Quaternion.Euler(direction).;

        if(Vector3.Distance(direction, Vector3.zero) > moveThreshold)
        {
            _player.OnMove();
        }
        else
        {
            _player.OnStop();
        }

        transform.Translate(direction * moveSpeed * Time.fixedDeltaTime, Space.World);
        //nếu lên cầu mà vượt quá limit cho phép thì lùi lại ngay
        if (transform.position.z > _player.BridgeStepLimitZ)
        {
            transform.position = new Vector3(_player.transform.position.x, _player.transform.position.y, _player.BridgeStepLimitZ);
        }
        else if(transform.position.z < _player.MinZ)
        {
            transform.position = new Vector3(_player.transform.position.x, _player.transform.position.y, _player.MinZ);
        }

        //chỉnh hướng mặt player
        if(direction != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(direction, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotateSpeed * Time.fixedDeltaTime);
        }

        // reset Rotation Camera?

    }


}