using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : Character
{
    protected override void Start()
    {
        base.Start();  
    }
    private float _minZ = float.MinValue;
    private float _bridgeStepLimitZ = float.MaxValue;
    private bool _canMove = true;
    private bool _isRunning = false;

    public bool CanMove => _canMove;

    public float MinZ
    {
        get => _minZ;
        set => _minZ = value;
    }
    public float BridgeStepLimitZ
    {
        get => _bridgeStepLimitZ;
        set => _bridgeStepLimitZ = value;
    }

    public override void OnRunOutOfStack()
    {
        base.OnRunOutOfStack();
        _bridgeStepLimitZ = transform.position.z - 0.1f;
    }

    public override void FailStopMoving()
    {
        base.FailStopMoving();
        _canMove = false;
    }

    public void OnMove()
    {
        if (!_isRunning)
        {
            _isRunning = true;
            ChangeAnim("Run");
        }
    }

    public void OnStop()
    {
        if (_isRunning)
        {
            _isRunning = false;
            ChangeAnim("Idle");
        }

    }

    public override void OnVictory(Transform centerPoint)
    {
        _canMove = false;
        base.OnVictory(centerPoint);
    }
}
