﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Kéo thả đúng thứ tự
    [SerializeField] private List<FloorController> floorList;
    [SerializeField] private List<Character> characterList;

    public List<FloorController> FloorList => floorList;
    public List<Character> CharacterList => characterList;
    public static GameManager Instance;

    private void Awake()
    {
        if(Instance == null || Instance != this)
        {
            Instance = this;
        }
    }

    public void StopAllCharacter(Character exception)
    {
        foreach(Character character in characterList)
        {
            if(character != exception)
            {
                character.FailStopMoving();
            }
        }
    }
}
