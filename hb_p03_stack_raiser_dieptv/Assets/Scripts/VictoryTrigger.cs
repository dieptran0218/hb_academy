using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryTrigger : MonoBehaviour
{
    [SerializeField] private Transform centerPoint;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Enemy"))
        {
            Character character = other.GetComponent<Character>();
            GameManager.Instance.StopAllCharacter(character);
            character.OnVictory(centerPoint);
            gameObject.SetActive(false);
        }
    }
}
