using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepDetectController : MonoBehaviour
{

    private BridgeController parentBridge;
    private Vector3 translation;
    private void Awake()
    {
        parentBridge = transform.parent.GetComponent<BridgeController>();
        translation = new Vector3(0, parentBridge.RootStack.localScale.z * Mathf.Sin(Mathf.Deg2Rad * parentBridge.BridgeAngle), parentBridge.RootStack.localScale.z * Mathf.Cos(Mathf.Deg2Rad * parentBridge.BridgeAngle));
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag.Equals("Player") || other.tag.Equals("Enemy"))
        {
            //Debug.Log("Detect Character");
            Character character = other.GetComponent<Character>();
            if(character.StackQueue.Count > 0)
            {
                if(parentBridge.StackList.Count < parentBridge.MaxStackCount)
                {
                    character.LayStack(parentBridge);
                    parentBridge.RootStack.localPosition += translation;
                    transform.localPosition += translation;
                }

            }
            else if (character.CompareTag("Enemy"))
            {
                if (parentBridge.StackList.Count < parentBridge.MaxStackCount)
                {
                    character.OnRunOutOfStack();
                }
            }

            if(parentBridge.StackList.Count >= parentBridge.MaxStackCount){
                character.OnUnlockBridge();
                gameObject.SetActive(false);
            }
        }
    }
}
