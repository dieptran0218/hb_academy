using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorController : MonoBehaviour
{
    [SerializeField] private List<BridgeController> bridgeList;
    [SerializeField] private StackGenerator stackGenerator;

    public List<BridgeController> BridgeList => bridgeList;

    public Vector3 GetRandomTarget(EnemyController enemy)
    {
        List<Vector2> indexList = new List<Vector2>();
        for(int i = 0; i<stackGenerator.Row; ++i)
        {
            for(int j = 0; j < stackGenerator.Column; ++j)
            {
                if(stackGenerator.StackTypeMatrix[i,j] == (int)enemy.m_StackType)
                {
                    indexList.Add(new Vector2(i, j));
                }
            }
        }
        Vector2 randomIndex;
        do
        {
            randomIndex = indexList[Random.Range(0, indexList.Count)];
        }
        while (Vector2.Distance(randomIndex, enemy.LastPickUpStackIndex) < 0.1f);
        enemy.LastPickUpStackIndex = randomIndex;
        return stackGenerator.LeftTopCorner.position + new Vector3(randomIndex.y * stackGenerator.DistanceX, -0.375f, -1 * randomIndex.x * stackGenerator.DistanceY);
    }
}
