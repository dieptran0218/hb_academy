﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour
{
    private BridgeController parentBridge;
    private void Awake()
    {
        parentBridge = transform.parent.GetComponent<BridgeController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //nếu là charater và va chạm ở vị trí thấp hơn cổng
        if (other.tag.Equals("Player") && other.transform.position.z < transform.position.z)
        {
            //Debug.Log("Detect Character");
            PlayerController player = other.GetComponent<PlayerController>();
            player.BridgeStepLimitZ = float.MaxValue;
        }
    }
}
