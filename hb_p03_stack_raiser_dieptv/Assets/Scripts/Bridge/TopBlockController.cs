using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopBlockController : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && other.transform.position.z >= transform.position.z)
        {
            PlayerController player = other.GetComponent<PlayerController>();
            player.MinZ = transform.position.z;
        }
    }
}
