﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeController : MonoBehaviour
{
    [SerializeField] private float bridgeAngle;
    [SerializeField] private Transform stackHolder,rootStack;
    [SerializeField] private int maxStackCount;
    [SerializeField] private Transform endPoint;
    private bool _isComplete = false;
    private StackType _stackType;
    private List<StackController> stackList;
    public float BridgeAngle => bridgeAngle;
    public Transform RootStack => rootStack;
    public StackType m_StackType => _stackType;
    public int MaxStackCount => maxStackCount;
    public List<StackController> StackList => stackList;
    public Transform EndPoint => endPoint;

    public bool IsComplete {
        get => _isComplete;
        set => _isComplete = value;
    }

    private void Awake()
    {
        stackList = new List<StackController>();
    }

    public void LayStack(StackController newStack)
    {
        if(stackList.Count == 0 && _stackType != newStack.StackType)
        {
            _stackType = newStack.StackType;
        }
        newStack.ParentBridge = this;
        newStack.IndexInBridgeList = stackList.Count; //ở đây count = index luôn vì add Stack sau khi set index
        stackList.Add(newStack);
        if(stackList.Count >= maxStackCount)
        {
            _isComplete = true;
        }
        newStack.transform.parent = stackHolder;
        DOTween.Sequence()
            .Append(newStack.transform.DOLocalMove(rootStack.localPosition, 0.15f))
            .Join(newStack.transform.DOLocalRotateQuaternion(rootStack.localRotation, 0.15f))
            .Join(newStack.transform.DOScale(rootStack.localScale, 0.15f));
    }

    public int Evaluate(int stackCount, StackType enemyStackType)
    {
        int score = stackCount;
        for(int i = 0; i<stackList.Count; ++i)
        {
            if(stackList[i].StackType == enemyStackType)
            {
                ++score;
            }
            else
            {
                --score;
            }
        }
        return score - (maxStackCount - stackList.Count);
    }
}
