﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StackGenerator : MonoBehaviour
{
    [SerializeField] Transform leftTopCorner, rightBottomCorner, stackParent;
    [SerializeField] int row, column;
    [SerializeField] List<GameObject> stackPrefabs; //cần kéo thả đúng thứ tự R B G Y
    [SerializeField] float respawnInterval;

    private List<Vector2> indexList;
    private int[,] stackTypeMatrix;
    private float distanceX, distanceY;

    public int Row => row;
    public int Column => column;
    public int[,] StackTypeMatrix => stackTypeMatrix;

    public Transform LeftTopCorner => leftTopCorner;
    public float DistanceX => distanceX;
    public float DistanceY => distanceY;
    // Start is called before the first frame update
    void Start()
    {
        indexList = new List<Vector2>();
        stackTypeMatrix = new int[row, column];
        distanceX = (rightBottomCorner.localPosition.x - leftTopCorner.localPosition.x) / (column - 1f);
        distanceY = (leftTopCorner.localPosition.z - rightBottomCorner.localPosition.z) / (row - 1f);

        GenerateStacks();
    }

    private void GenerateStacks()
    {
        for(int i = 0; i<row; ++i)
        {
            for (int j = 0; j < column; ++j)
            {
                indexList.Add(new Vector2(i, j));
            }
        }

        int generatedStack = 0;
        for(int i = 0; i<stackPrefabs.Count; ++i)
        {
            int stackQuantity = row * column / stackPrefabs.Count;
            generatedStack += stackQuantity;
            for(int j = 0; j < stackQuantity; ++j)
            {
                int randomIndex = UnityEngine.Random.Range(0, indexList.Count);
                Vector3 stackPosition = leftTopCorner.localPosition + new Vector3(indexList[randomIndex].y * distanceX, -0.375f, -1 * indexList[randomIndex].x * distanceY);
                StackController newStack = Instantiate(stackPrefabs[i], stackPosition, Quaternion.identity, stackParent).GetComponent<StackController>();
                newStack.transform.localPosition = stackPosition;
                newStack.transform.localScale = Vector3.zero;
                newStack.gameObject.SetActive(true);
                newStack.transform.DOScale(new Vector3(1f, 0.25f, 0.5f), 0.2f);
                newStack.Index = indexList[randomIndex];
                newStack.m_StackGenerator = this;
                stackTypeMatrix[(int)indexList[randomIndex].x, (int)indexList[randomIndex].y] = i;
                indexList.RemoveAt(randomIndex);
            }
        }

        while(indexList.Count > 0)
        {
            int randomIndex = UnityEngine.Random.Range(0, indexList.Count);
            StackType randomType = stackPrefabs[randomIndex].GetComponent<StackController>().StackType;
            Vector3 stackPosition = leftTopCorner.localPosition + new Vector3(indexList[randomIndex].y * distanceX, -0.375f, -1 * indexList[randomIndex].x * distanceY);
            StackController newStack = Instantiate(stackPrefabs[(int)randomType], stackPosition, Quaternion.identity, stackParent).GetComponent<StackController>();
            newStack.transform.localPosition = stackPosition;
            newStack.transform.localScale = Vector3.zero;
            newStack.gameObject.SetActive(true);
            newStack.transform.DOScale(new Vector3(1f, 0.25f, 0.5f), 0.2f);
            newStack.Index = indexList[randomIndex];
            newStack.m_StackGenerator = this;
            stackTypeMatrix[(int)indexList[randomIndex].x, (int)indexList[randomIndex].y] = (int)randomType;
            indexList.RemoveAt(randomIndex);
        }
    }

    public void RespawnDelay(Vector2 index)
    {
        DOTween.Sequence()
            .AppendInterval(respawnInterval)
            .AppendCallback(() =>
            {
                //Debug.Log("Finished respawning stack at index " + index.x + " - " + index.y);
                Vector3 stackPosition = leftTopCorner.localPosition + new Vector3(index.y * distanceX, -0.375f, -1 * index.x * distanceY);
                StackController newStack = Instantiate(stackPrefabs[stackTypeMatrix[(int)index.x, (int)index.y]], stackPosition, Quaternion.identity, stackParent).GetComponent<StackController>();
                newStack.Index = index;
                newStack.transform.localPosition = stackPosition;
                newStack.transform.localScale = Vector3.zero;
                newStack.gameObject.SetActive(true);
                newStack.transform.DOScale(new Vector3(1f, 0.25f, 0.5f), 0.2f);
                newStack.m_StackGenerator = this;
            });
    }

}
