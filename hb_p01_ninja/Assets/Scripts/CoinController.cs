using UnityEngine;
using DG.Tweening;

public class CoinController : MonoBehaviour
{
    public void OnPlayerTouch()
    {
        SpriteRenderer theImage = GetComponent<SpriteRenderer>();
        DOTween.Sequence()
            .Append(transform.DOLocalMoveY(2f, 0.5f).SetEase(Ease.OutCubic))
            .Append(theImage.DOFade(0,0.2f).SetEase(Ease.Linear))   //TODO
            .AppendCallback(()=> {
                gameObject.SetActive(false);
            });
    }
}
