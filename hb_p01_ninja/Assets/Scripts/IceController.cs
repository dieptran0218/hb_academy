using UnityEngine;

public class IceController : MonoBehaviour
{
    [SerializeField] private int iceDamage;
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Player"))
        {
            PlayerController.Instance.TakeDamage(iceDamage);
        }
    }
}
