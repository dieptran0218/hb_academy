using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class KunaiPoolController : MonoBehaviour
{
    
    public static KunaiPoolController Instance { get; private set; }
    private void Awake() 
    {
        if (Instance != null && Instance != this) 
        { 
            Destroy(this); 
        } 
        else 
        { 
            Instance = this; 
        } 
    }

    [SerializeField] private GameObject kunaiPrefab;
    [SerializeField] private int kunaiPoolInitSize;

    private Queue<KunaiProjectileController> kunaiQueue;

    private void Start()
    {
        for (int i = 0; i < kunaiPoolInitSize; ++i)
        {
            kunaiQueue.Enqueue(Instantiate(kunaiPrefab,transform).GetComponent<KunaiProjectileController>());
        }
    }

    public KunaiProjectileController GetKunai()
    {
        if (kunaiQueue.Count <= 0)
        {
            return Instantiate(kunaiPrefab, transform).GetComponent<KunaiProjectileController>();
        }
        else
        {
            return kunaiQueue.Dequeue();
        }
    }

    public void ReturnKunai(KunaiProjectileController deadKunai)
    {
        deadKunai.gameObject.SetActive(false);
        kunaiQueue.Enqueue(deadKunai);
    }
}
