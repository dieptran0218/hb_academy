using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> levelPrefabs;
    [SerializeField] private Image screenCover;
    private GameObject currentLevelObject;
    private Transform enemyParent;
    private int enemyCount;
    private int currentLevel = 1;
    
    public bool isPlaying;
    public static GameManager Instance;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        SoundManager.Instance.PlaySound("Background_Music","BG", true);
        LoadLevel(1);
    }

    public void OnEnemyKilled()
    {
        if(--enemyCount == 0)
        {
            SoundManager.Instance.InvokePlaySound("Victory", "Player_SFX", false, 1f);
        }
    }

    private void LoadLevel(int level)
    {
        if (level < levelPrefabs.Count)
        {
            DOTween.Sequence()
                .Append(screenCover.DOFade(1f, 0.75f))
                .AppendCallback(() =>
                {
                    if (currentLevelObject != null)
                    {
                        Destroy(currentLevelObject);
                    }
                    currentLevelObject = Instantiate(levelPrefabs[level - 1], Vector3.zero, Quaternion.identity, null);
                })
                .Append(screenCover.DOFade(0f, 0.75f));
        }
        else
        {
            Debug.LogError("Level is not available :" + level);
        }
    }

    public void LoadNextLevel()
    {
        if (currentLevel + 1 <= levelPrefabs.Count)
        {
            DOTween.Sequence()
                .Append(screenCover.DOFade(1f, 0.5f))
                .AppendCallback(() =>
                {
                    if (currentLevelObject != null)
                    {
                        Destroy(currentLevelObject);
                    }
                    currentLevelObject = Instantiate(levelPrefabs[++currentLevel - 1],Vector3.zero,Quaternion.identity,null);
                })
                .Append(screenCover.DOFade(0f, 0.5f));
        }
        else
        {
            Debug.LogError("Level is not available :" + (currentLevel + 1));
            Application.Quit();
        }
    }
    public void SetEnemyParent(Transform newEnemyParent)
    {
        enemyParent = newEnemyParent;
        enemyCount = enemyParent.childCount;
    }
}
