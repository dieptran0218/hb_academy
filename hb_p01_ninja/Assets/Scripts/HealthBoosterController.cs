using DG.Tweening;
using UnityEngine;

enum BoosterType
{
    FullHealth,
    Power
}

public class HealthBoosterController : MonoBehaviour
{
    [SerializeField] private BoosterType type;
    private Tween idleTweening;
    private float originalPositionY;

    // Start is called before the first frame update
    void Start()
    {
        originalPositionY = transform.position.y;
        idleTweening = transform.DOScale(0.8f, 0.3f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            switch (type)
            {
                case BoosterType.Power:
                    PlayerController.Instance.OnGetPowerBooster();
                    break;
                case BoosterType.FullHealth:
                    PlayerController.Instance.OnGetHealthBooster();
                    break;
            }

            SoundManager.Instance.PlaySound("Health_Booster", "Environment_SFX");
            idleTweening.Kill();
            SpriteRenderer image = GetComponent<SpriteRenderer>();
            DOTween.Sequence()
                .Append(transform.DOMoveY(originalPositionY + 2.7f, 0.5f))
                .Join(transform.DOScale(1.3f, 0.5f))
                .Append(transform.DOScale(1f, 0.25f))
                .Join(transform.DOMoveY(originalPositionY + 2.3f, 0.25f))
                .Append(transform.DOMoveY(originalPositionY + 2.8f, 0.5f))
                .Join(image.DOFade(0, 0.25f).SetEase(Ease.Linear))
                .AppendCallback(() =>
                {
                    Destroy(gameObject);
                });
        }
    }
}
