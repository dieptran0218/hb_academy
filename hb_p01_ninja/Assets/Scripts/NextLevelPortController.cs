using UnityEngine;
using DG.Tweening;
public class NextLevelPortController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag.Equals("Player"))
        {
            GameManager.Instance.LoadNextLevel();
            PlayerController.Instance.OnNextLevelTeleportStarted();
        }
    }
}
