using UnityEngine;

public class TeleportController : MonoBehaviour
{
    [SerializeField] private Transform inPortMask;
    [SerializeField] private Transform outPortMask;

    private void Start()
    {
        inPortMask.gameObject.SetActive(false);
        outPortMask.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Player"))
        {
            inPortMask.gameObject.SetActive(true);
            outPortMask.gameObject.SetActive(true);
            PlayerController.Instance.Teleport(inPortMask,outPortMask,3f);
        }
    }

}
