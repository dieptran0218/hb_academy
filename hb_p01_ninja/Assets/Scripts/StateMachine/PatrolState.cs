using UnityEngine;

public class PatrolState : IState
{
    float randomTime, timer;
    public void OnEnter(EnemyController enemy)
    {
        timer = 0;
        randomTime = Random.Range(3f, 6f);
    }

    public void OnExecute(EnemyController enemy)
    {
        timer += Time.deltaTime;
        if(enemy.Target != null)
        {
            enemy.SetDirection(enemy.transform.position.x > enemy.Target.transform.position.x);
            if (enemy.IsTargetInRange())
            {
                enemy.ChangeState(new AttackState());
            }
            else
            {
                enemy.Moving();
            }

        }
        else
        {
            if (timer < randomTime)
            {
                enemy.Moving();
            }
            else
            {
                enemy.ChangeState(new IdleState());
            }
        }

    }

    public void OnExit(EnemyController enemy)
    {

    }
}
