using UnityEngine;

public class AttackState : IState
{
    float timer;
    public void OnEnter(EnemyController enemy)
    {
        if(enemy.Target != null)
        {
            enemy.SetDirection(enemy.transform.position.x > enemy.Target.transform.position.x);
            enemy.StopMoving(false);
            enemy.Attack();
        }
        timer = 0;
    }

    public void OnExecute(EnemyController enemy)
    {
        timer += Time.deltaTime;
        if (timer > 1f / enemy.AttackSpeed)
        {
            enemy.ChangeState(new PatrolState());
        }
    }

    public void OnExit(EnemyController enemy)
    {

    }
}
