using UnityEngine;
using DG.Tweening;

public class CheckpointController : MonoBehaviour
{
    [SerializeField] Sprite flippedImage;
    private bool isReached = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Flip()
    {
        if (!isReached)
        {
            isReached = true;

            SpriteRenderer theImage = GetComponent<SpriteRenderer>();
            DOTween.Sequence()
                .Append(transform.DORotate(new Vector3(0, 180, 0), 0.5f).SetEase(Ease.Linear))
                .AppendCallback(() => {
                    if (theImage != null)
                    {
                        transform.rotation = Quaternion.identity;
                        theImage.sprite = flippedImage;
                    }
                });
        }

    }
}
