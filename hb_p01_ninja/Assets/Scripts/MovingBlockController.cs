using UnityEngine;
using DG.Tweening;

public class MovingBlockController : MonoBehaviour
{
    [SerializeField] private Transform startPoint;
    [SerializeField] private Transform endPoint;
    [SerializeField] private float duration;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = startPoint.position;
        transform.DOMove(endPoint.position, duration).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
    }
}
