using UnityEngine;

public class SwingAreaController : MonoBehaviour
{
    [SerializeField] CharacterController owner;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player") || collision.tag.Equals("Enemy"))
        {
            owner.AddVictimInSwingRange(collision.GetComponent<CharacterController>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        owner.RemoveVictimInSwingRange(collision.GetComponent<CharacterController>());
    }
}
