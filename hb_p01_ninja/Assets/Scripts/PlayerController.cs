﻿ using DG.Tweening;
 using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : CharacterController
{
    public static PlayerController Instance;
    [SerializeField] private float maxSwingCooldown;
    [SerializeField] private float maxThrowCooldown;
    [SerializeField] private float kunaiMoveSpeed;
    [SerializeField] private float kunaiDamage;
    [SerializeField] private GameObject kunaiPrefab;
    [SerializeField] private Transform kunaiThrowPoint;
    [SerializeField] private Transform kunaiParent;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private float horizontalDelta;
    [SerializeField] private float horizontalSpeed;

    [SerializeField] private TextMeshProUGUI coinText;

    //[SerializeField] private float verticalDelta;
    //[SerializeField] private float verticalSpeed;
    [SerializeField] private float jumpForce;

    private bool isOnGround;
    private bool isUnderground;
    private bool isJumping;
    private bool isTeleporting;

    private Vector3 checkpoint = Vector3.zero;

    //private bool isOnAir;
    //private bool isJumpOut;
    private int coin;
    private float currentSwingCooldown;
    private float currentThrowCooldown;
    private bool isAttack;
    private bool isAttackButtonPressed;
    private bool isJumpButtonPressed;
    private bool isLeftButtonPressed;
    private bool isRightButtonPressed;
    private bool isThrowButtonPressed;
    private bool isThrowing;
    private Transform parentTransform;

    // Start is called before the first frame update
    private Rigidbody2D rb;
    public Rigidbody2D RB => rb;
    public bool IsOnGround => isOnGround;

    private void Awake()
    {
        Instance = this;
        parentTransform = transform.parent;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (IsDead) return;
        currentThrowCooldown -= Time.deltaTime;
        currentSwingCooldown -= Time.deltaTime;
        isOnGround = CheckOnGround();
        isUnderground = CheckUnderground();
        horizontalDelta = Input.GetAxisRaw("Horizontal");
        //verticalDelta = Input.GetAxisRaw("Vertical");
        //TODO
        if (isOnGround && rb.velocity.y < -0.1f)
        {
            isJumping = false;
        }
        //if (isOnGround && rb.velocity.y < 0)
        //{
        //    Debug.Log("false");
        //    isJumping = false;
        //}
        
        if(isTeleporting) return;

        if (Input.GetKey(KeyCode.Space) || isJumpButtonPressed && isOnGround)
        {
            Jump();
        }

        if (Input.GetKey(KeyCode.C) || isAttackButtonPressed)
        {
            Attack();
        }

        if (Input.GetKey(KeyCode.V) || isThrowButtonPressed)
        {
            Throw();
        }

        if (Mathf.Abs(horizontalDelta) > 0.1f  && (!isAttack && !isThrowing || !isOnGround))
        {
            rb.velocity = new Vector2(horizontalDelta * horizontalSpeed, rb.velocity.y);

            if(rb.velocity.x < 0)
            {
                transform.rotation = Quaternion.Euler(0, -180, 0);
                currentHeathBar.fillOrigin = (int)Image.OriginHorizontal.Right;
            }
            else
            {
                transform.rotation = Quaternion.identity;
                currentHeathBar.fillOrigin = (int)Image.OriginHorizontal.Left;
            }
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }

        if(isRightButtonPressed && !(isAttack || isThrowing))
        {
            rb.velocity = new Vector2(horizontalSpeed, rb.velocity.y);
            transform.rotation = Quaternion.identity;
            currentHeathBar.fillOrigin = (int)Image.OriginHorizontal.Left;
        }

        if (isLeftButtonPressed && !(isAttack || isThrowing))
        {
            rb.velocity = new Vector2(-1 * horizontalSpeed, rb.velocity.y);
            transform.rotation = Quaternion.Euler(0, -180, 0);
            currentHeathBar.fillOrigin = (int)Image.OriginHorizontal.Right;
        }

        //if (Mathf.Abs(verticalDelta) > 0.1f)
        //{
        //    rb.velocity = new Vector2(rb.velocity.x, verticalDelta * verticalSpeed);
        //}
        //else
        //{
        //    rb.velocity = new Vector2(rb.velocity.x, 0);
        //}

        if (isOnGround)
        {
            if(!isThrowing && !isAttack)
            {
                if (Mathf.Abs(rb.velocity.x) > 0.1f)
                {
                    if (Mathf.Abs(rb.velocity.y) < 2f)    //fix lỗi khi đang nhảy lướt qua Ground sẽ trigger Run
                    {
                        ChangeAnim("Run");
                    }
                }
                else if (!isJumping)
                {
                    ChangeAnim("Idle");
                }
            }

        }
        else
        {
            if(rb.velocity.y < 0.1f && !isJumping)
            {
                ChangeAnim("Jump_out");
                //isJumping = true;
            }
        }

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Coin"))
        {
            CoinController theCoin = collision.gameObject.GetComponent<CoinController>();
            if(theCoin != null)
            {
                theCoin.OnPlayerTouch();
                coinText.text = ++coin + "";
                SoundManager.Instance.PlaySound("Coin_Pickup", "Environment_SFX");
            }
        }
        else if(collision.tag.Equals("DeathZone"))
        {
            TakeDamage(MaxHP);
        }
        else if (collision.tag.Equals("Checkpoint"))
        {
            CheckpointController newCheckpoint = collision.GetComponent<CheckpointController>();
            if(newCheckpoint != null)
            {
                OnCheckpointReached(newCheckpoint);
            }
            else
            {
                Debug.LogError("Null Checkpoint");
            }

        }
    }

    public override void OnInit()
    {
        base.OnInit();
        checkpoint = transform.position;
        coinText.text = coin + "";
        //isJumpOut = false;
        isAttack = false;
        // isRunning = false;
        isOnGround = false;
        isUnderground = false;
        ChangeAnim("Jump_out");
        rb.simulated = true;
        currentThrowCooldown = -1f;
        currentSwingCooldown = -1f;
    }

    public override void TakeDamage(float damageTaken)
    {
        base.TakeDamage(damageTaken);
        if(currentHP <= 0)
        {
            SoundManager.Instance.PlaySound("Death", "Player_SFX");
        }
    }

    public override void OnDeath()
    {
        base.OnDeath();
        Die();
    }

    private void Attack()
    {
        if (!(isAttack || isThrowing) && currentSwingCooldown < 0)
        {
            SoundManager.Instance.PlaySound("Attack", "Player_SFX");
            isAttack = true;
            currentSwingCooldown = maxSwingCooldown;

            ChangeAnim(isOnGround ? "Attack" : "Jump_Attack");

            DealSwingDamage();
            Invoke(nameof(OnAttackDone), 0.35f);
        }
    }

    private void Throw()
    {
        if (!(isThrowing || isAttack) && currentThrowCooldown < 0)
        {
            SoundManager.Instance.PlaySound("Throw", "Player_SFX");
            isThrowing = true;
            currentThrowCooldown = maxThrowCooldown;
            float delay = 0.1f;
            if (isOnGround)
            {
                ChangeAnim("Throw");
            }
            else
            {
                delay = 0.3f;
                ChangeAnim("Jump_Throw");
            }
            DOTween.Sequence()
                .AppendInterval(delay)
                .AppendCallback(() =>
                {
                    GameObject newKunai = Instantiate(kunaiPrefab, kunaiThrowPoint.position, transform.rotation.eulerAngles.y == 0 ? Quaternion.identity : Quaternion.Euler(0, -180, 0), kunaiParent);
                    newKunai.GetComponent<KunaiProjectileController>().Init(kunaiMoveSpeed, transform.rotation.eulerAngles.y == 0, kunaiDamage);
                })
                .AppendInterval(0.517f)
                .AppendCallback(OnThrowDone);
        }

    }

    private void Jump()
    {
        if (!isJumping && !isUnderground)
        {
            SoundManager.Instance.PlaySound("Jump", "Player_SFX");
            isJumping = true;
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(jumpForce * Vector2.up);
            //Debug.Log("Jump");
            ChangeAnim("Jump");
        }
    }

    public void OnMoveRightDown()
    {
        isRightButtonPressed = true;
    }

    public void OnMoveRightUp()
    {
        isRightButtonPressed = false;
    }

    public void OnMoveLeftDown()
    {
        isLeftButtonPressed = true;
    }

    public void OnMoveLeftUp()
    {
        isLeftButtonPressed = false;
    }

    public void OnJumpBtnDown()
    {
        isJumpButtonPressed = true;
    }

    public void OnJumpBtnUp()
    {
        isJumpButtonPressed = false;
    }

    public void OnAttackBtnDown()
    {
        isAttackButtonPressed = true;
    }

    public void OnAttackBtnUp()
    {
        isAttackButtonPressed = false;
    }

    public void OnThrowBtnDown()
    {
        isThrowButtonPressed = true;
    }

    public void OnThrowBtnUp()
    {
        isThrowButtonPressed = false;
    }

    private void OnAttackDone()
    {
        isAttack = false;
    }

    private void OnThrowDone() 
    {
        isThrowing = false;
    }

    private bool CheckUnderground()
    {
        // Debug.DrawLine(transform.position + Vector3.down * 0.9f, transform.position + Vector3.up * 1.05f, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.down * 0.9f, Vector2.up , 1f, groundLayer);

        return hit.collider != null;
    }

    private bool CheckOnGround()
    {
        // Debug.DrawLine(transform.position + Vector3.down * 0.9f , transform.position + Vector3.down * 1.3f, Color.green);
        // Debug.DrawLine(transform.position + Vector3.down * 1.1f + Vector3.left * 0.5f, transform.position + Vector3.down * 1.1f + Vector3.right * 0.5f, Color.blue);
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.down * 0.9f, Vector2.down, 0.3f, groundLayer);

        if(hit.collider == null)
        {
            hit = Physics2D.Raycast(transform.position + Vector3.down * 1.1f + Vector3.left * 0.5f, Vector2.right, 1f, groundLayer);
        }

        if(hit.collider != null && hit.collider.gameObject.tag.Equals("MovingBlock"))
        {
            transform.parent = hit.collider.transform;
            isJumping = false;
        }
        else
        {
            transform.parent = parentTransform;
        }

        return hit.collider != null;
    }

    private void OnCheckpointReached(CheckpointController newCheckpoint)
    {
        checkpoint = newCheckpoint.transform.position;
        newCheckpoint.Flip();
    }

    private void Die()
    {
        ChangeAnim("Die");
        DOTween.Sequence()
            .AppendInterval(1.5f)
            .AppendCallback(()=> {
                rb.simulated = false;
            })
            .AppendInterval(0.8f)
            .AppendCallback(RevivePlayer);
    }

    private void RevivePlayer()
    {
        transform.position = checkpoint + Vector3.up * 3f;
        BackgroundFollowController.Instance.OnPlayerChangePosition();
        OnInit();
    }

    public void Teleport(Transform input, Transform output, float runOutDistance)
    {
        isTeleporting = true;
        rb.simulated = false;
        ChangeAnim("Run");
        Vector3 outputPosition = output.position;
        DOTween.Sequence()
            .Append(transform.DOMoveX(input.position.x, 1f).SetEase(Ease.Linear))
            .AppendCallback(()=>
            {
                transform.position = outputPosition;
                BackgroundFollowController.Instance.OnPlayerChangePosition();
            })
            .Append(transform.DOMoveX(outputPosition.x + runOutDistance, 1f).SetEase(Ease.Linear))
            .AppendCallback(() =>
            {
                rb.simulated = true;
                isTeleporting = false;
                input.gameObject.SetActive(false);
                output.gameObject.SetActive(false);
            });
    }

    public void OnNextLevelTeleportStarted()
    {
        isTeleporting = true;
        rb.simulated = false;
        ChangeAnim("Run");
        transform.DOMoveX(transform.position.x + 1.5f, 0.49f).SetEase(Ease.Linear);
    }

    public void OnNextLevelTeleportDone()
    {
        isTeleporting = false;
        rb.simulated = true;
    }
    
    
    public void OnGetHealthBooster()
    {
        DOTween.To(() => currentHP, x => currentHP = x, MaxHP, 0.5f).SetEase(Ease.InCubic).OnUpdate(() =>
        {
            currentHeathBar.fillAmount = currentHP / MaxHP;
        });
    }

    public void OnGetPowerBooster()
    {
        DOTween.Sequence()
            .AppendCallback(() =>
            {
                Damage *= 1.25f;
                ActivateHealing(100f);
            })
            .AppendInterval(5f)
            .AppendCallback(() =>
            {
                Damage /= 1.25f;
            });
    }

    private void ActivateHealing(float healRate)
    {
        DOTween.Sequence()
            .AppendCallback(() =>
            {
                DOTween.To(() => currentHP, x => currentHP = x, currentHP + healRate, 0.5f).SetEase(Ease.InCubic)
                        .OnUpdate(() =>
                        {
                            currentHeathBar.fillAmount = currentHP / MaxHP;
                        });
            })
            .AppendInterval(1f)
            .SetLoops(5,LoopType.Restart);
    }
}
