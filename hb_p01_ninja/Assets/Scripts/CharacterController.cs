using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private SpriteRenderer image;
    [SerializeField] private float maxHP;
    protected float currentHP;
    [SerializeField] protected Image currentHeathBar;
    [SerializeField] protected GameObject combatTextPrefab;
    [SerializeField] private RectTransform combatTextParent;
    [SerializeField] private float damage;
    [SerializeField] protected SwingAreaController swingArea;
    private string currentAnim = "";
    [SerializeField] private GameObject bloodPrefab;
    [SerializeField] private Transform bloodSpawnPoint;
    [SerializeField] private Transform bloodEffectParent;
    protected List<CharacterController> victimInSwingRange;

    protected bool IsDead => currentHP <= 0;
    public float Damage
    {
        get => damage;
        set => damage = value;
    }
    public float MaxHP => maxHP;

    private void Start()
    {
        OnInit();
    }

    public virtual void OnInit()
    {
        currentHP = maxHP;
        currentHeathBar.fillAmount = 1f;
        currentHeathBar.transform.parent.gameObject.SetActive(true);
        victimInSwingRange = new List<CharacterController>();
    }

    public virtual void OnDespawn()
    {

    }

    public virtual void OnDeath()
    {
    }

    public virtual void TakeDamage(float DamageTaken)
    {
        if (IsDead) return;
        DOTween.Sequence()
            .AppendCallback(() =>
            {
                image.color = new Color32(255,65,65,255);
            })
            .AppendInterval(0.3f)
            .AppendCallback(() =>
            {
                image.color = Color.white;
            });

        currentHP -= DamageTaken;
        currentHeathBar.fillAmount = currentHP / maxHP;
        if(currentHP <= 0)
        {
            currentHeathBar.transform.parent.gameObject.SetActive(false);
            OnDeath();
        }

        CombatTextController newCombatText =  Instantiate(combatTextPrefab,transform.position + Vector3.up * 0.5f,Quaternion.identity, combatTextParent).GetComponent<CombatTextController>();
        newCombatText.Init(DamageTaken);
    }

    protected void ChangeAnim(string animName)
    {
        if (!anim.GetCurrentAnimatorClipInfo(0)[0].clip.name.Equals(animName))
        {
            currentAnim = animName;
            anim.ResetTrigger(animName);
            anim.SetTrigger(animName);
        }
    }

    public virtual void PlayBloodEffect(bool isRight)
    {
        GameObject bloodEffect = Instantiate(bloodPrefab, bloodSpawnPoint.position, isRight ? Quaternion.Euler(0, 90, 0) : Quaternion.Euler(0, -90, 0), bloodEffectParent);
        DOTween.Sequence()
            .AppendInterval(1f)
            .AppendCallback(() =>
            {
                Destroy(bloodEffect);
            });
    }

    public void AddVictimInSwingRange(CharacterController victim)
    {
        victimInSwingRange.Add(victim);
    }

    public void RemoveVictimInSwingRange(CharacterController victim)
    {
        victimInSwingRange.Remove(victim);
    }

    protected void DealSwingDamage()
    {
        for (int i = 0; i < victimInSwingRange.Count; ++i)
        {
            victimInSwingRange[i].TakeDamage(damage);
            victimInSwingRange[i].PlayBloodEffect(transform.rotation.y == 0);
        }
    }
}
