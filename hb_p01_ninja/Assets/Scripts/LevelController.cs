using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private Transform enemyParent;
    [SerializeField] private Transform startPosition;
    // Start is called before the first frame update
    private void Start()
    {
        GameManager.Instance.SetEnemyParent(enemyParent);
        PlayerController.Instance.transform.position = startPosition.position;
        PlayerController.Instance.OnNextLevelTeleportDone();
        GameManager.Instance.isPlaying = true;
        
    }
}
