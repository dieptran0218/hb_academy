using UnityEngine;

public class IceDropperController : MonoBehaviour
{
    [SerializeField] private Rigidbody2D iceRb;

    private void OnTriggerEnter2D(Collider2D Col)
    {
        if (Col.tag.Equals("Player"))
        {
            DropTheIce();
        }
    }

    private void DropTheIce()
    {
        iceRb.simulated = true;
    }
}