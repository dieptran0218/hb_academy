using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : CharacterController
{
    [SerializeField] private float attackRange;
    [SerializeField] private float attackSpeed;
    [SerializeField] private float moveSpeed;
    [SerializeField] private Transform leftLimit;
    [SerializeField] private Transform rightLimit;

    private bool isRunningRight = true;
    private Rigidbody2D rb;
    private PlayerController target;

    public IState currentState;

    public PlayerController Target => target;
    public float AttackSpeed => attackSpeed;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        if (currentState != null)
        {
            currentState.OnExecute(this);
        }
    }

    public override void OnInit()
    {
        base.OnInit();
        ChangeState(new IdleState());
    }

    public override void OnDespawn()
    {
        base.OnDespawn();
    }

    public override void OnDeath()
    {
        base.OnDeath();
        GameManager.Instance.OnEnemyKilled();
        Die();
    }

    public override void TakeDamage(float DamageTaken)
    {
        base.TakeDamage(DamageTaken);
    }

    private void Die()
    {
        DOTween.Sequence()
            .AppendCallback(() =>
            {
                ChangeAnim("Die");
            })
            .AppendInterval(1f)
            .AppendCallback(() =>
            {
                gameObject.SetActive(false);
            });
    }

    public void ChangeState(IState newState)
    {
        if (currentState != null)
        {
            currentState.OnExit(this);
        }

        currentState = newState;
        //Debug.Log(newState.ToString());
        if (currentState != null)
        {
            currentState.OnEnter(this);
        }
    }

    public void Moving()
    {
        if (IsDead) return;
        ChangeAnim("Run");
        if (isRunningRight)
        {
            rb.velocity = Vector2.right * moveSpeed;
        }
        else
        {
            rb.velocity = Vector2.left * moveSpeed;
        }

        if (transform.position.x < leftLimit.position.x && target == null)
        {
            TurnRight();
        }
        if(transform.position.x > rightLimit.position.x && target == null)
        {
            TurnLeft();
        }
    }

    private void TurnLeft()
    {
        if (isRunningRight)
        {
            isRunningRight = false;
            transform.rotation = Quaternion.Euler(0, -180, 0);
            currentHeathBar.fillOrigin = (int)Image.OriginHorizontal.Right;
        }
    }

    private void TurnRight()
    {
        if (!isRunningRight)
        {
            isRunningRight = true;
            transform.rotation = Quaternion.identity;
            currentHeathBar.fillOrigin = (int)Image.OriginHorizontal.Left;
        }
    }

    public void SetDirection(bool isRight)
    {
        if (IsDead) return;
        if (!isRight)
        {
            isRunningRight = true;
            transform.rotation = Quaternion.identity;
            currentHeathBar.fillOrigin = (int)Image.OriginHorizontal.Left;
        }
        else
        {
            isRunningRight = false;
            transform.rotation = Quaternion.Euler(0, -180, 0);
            currentHeathBar.fillOrigin = (int)Image.OriginHorizontal.Right;
        }
    }

    public void StopMoving(bool setIdle)
    {
        if (IsDead) return;
        if (setIdle)
        {
            ChangeAnim("Idle");
        }
        rb.velocity = Vector2.zero;
    }

    public void Attack()
    {
        if (IsDead) return;
        SoundManager.Instance.PlaySound("Attack", "Enemy_SFX");
        ChangeAnim("Attack");
        DealSwingDamage();
    }

    public void SetTarget(PlayerController player)
    {
        this.target = player;
        if(target != null)
        {
            if (IsTargetInRange())
            {
                ChangeState(new AttackState());
            }
            else
            {
                ChangeState(new PatrolState());
            }
        }

    }

    public bool IsTargetInRange()
    {
        return Vector2.Distance(transform.position, target.transform.position) < attackRange;
    }
}
