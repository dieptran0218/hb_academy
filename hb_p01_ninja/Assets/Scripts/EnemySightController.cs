using UnityEngine;

public class EnemySightController : MonoBehaviour
{
    [SerializeField] private EnemyController owner;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            //Debug.Log("Found Player");
            owner.SetTarget(PlayerController.Instance);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            owner.SetTarget(null);
        }
    }
}
