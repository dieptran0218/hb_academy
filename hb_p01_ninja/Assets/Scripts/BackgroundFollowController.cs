using UnityEngine;

public class BackgroundFollowController : MonoBehaviour
{
    [SerializeField] float activeRate, inactiveRate;
    private Rigidbody2D rb;
    private bool isJustStop = false;
    [SerializeField]private Vector3 originalPlayerPosition;
    [SerializeField] private Vector3 originalBGPosition;

    public static BackgroundFollowController Instance;
    // Update is called once per frame
    private void Start()
    {
        Instance = this;
        rb = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        if(PlayerController.Instance.RB.velocity.y == 0)
        {
            rb.velocity = Vector2.zero;
            if (PlayerController.Instance.IsOnGround)
            {
                if (!isJustStop)
                {
                    isJustStop = true;
                    originalPlayerPosition = PlayerController.Instance.transform.position;
                    originalBGPosition = transform.position;
                }
                else
                {
                    transform.position = Vector3.Lerp(originalBGPosition, new Vector3(originalBGPosition.x, originalBGPosition.y + PlayerController.Instance.transform.position.y - originalPlayerPosition.y, originalBGPosition.z), inactiveRate);
                }
            }

        }
        else
        {
            isJustStop = false;
            rb.velocity = PlayerController.Instance.RB.velocity * activeRate;
        }

    }

    public void OnPlayerChangePosition()
    {
        transform.position = Vector3.Lerp(originalBGPosition, new Vector3(originalBGPosition.x, originalBGPosition.y + PlayerController.Instance.transform.position.y - originalPlayerPosition.y, originalBGPosition.z), inactiveRate);
    }
}
