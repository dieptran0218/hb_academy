using UnityEngine;
using DG.Tweening;
using TMPro;

public class CombatTextController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI combatText;
    // Start is called before the first frame update
    
    public void Init(float ownerDamage)
    {
        float originalPostitionY = combatText.rectTransform.position.y;
        DOTween.Sequence()
            .AppendCallback(() =>
            {
                combatText.text = (int)ownerDamage + "";
                combatText.enabled = true;
            })
            .Append(combatText.transform.DOMoveY(originalPostitionY + 2f, 0.7f).SetEase(Ease.OutCubic))
            .Append(combatText.DOFade(0, 0.3f).SetEase(Ease.Linear))
            .AppendCallback(() =>
            {
                Destroy(gameObject);
            });
    }
}
