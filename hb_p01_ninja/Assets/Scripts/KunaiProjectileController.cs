using UnityEngine;

public class KunaiProjectileController : MonoBehaviour
{
    private float damage;
    // Start is called before the first frame update
    public void Init(float moveSpeed, bool isRight, float ownerDamage)
    {
        this.damage = ownerDamage;
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.velocity = isRight ? Vector2.right * moveSpeed : Vector2.left * moveSpeed;
        Invoke(nameof(OnSelfDestroy), 2f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Enemy"))
        {
            EnemyController enemy = collision.GetComponent<EnemyController>();
            enemy.TakeDamage(damage);
            enemy.PlayBloodEffect(transform.rotation.y == 0);
            OnSelfDestroy();
        }
    }

    private void OnSelfDestroy()
    {
        if (gameObject != null)
        {
            Destroy(gameObject);
        }
    }
}
