using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private List<AudioClip> listAudioClip;

    [SerializeField] private List<AudioSource> listAudioSource;


    public static SoundManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void PlaySound(string ClipName, string AudioSourceName, bool IsLoop = false, float Delay = 0f)
    {
        int audioSourceIndex = -1;
        for (int i = 0; i < listAudioSource.Count; i++)
        {
            if (AudioSourceName == listAudioSource[i].name)
            {
                audioSourceIndex = i;
            }
        }

        if (audioSourceIndex == -1)
        {
            Debug.Log("AudioSource not found: " + AudioSourceName);
            return;
        }

        foreach (AudioClip eachAudioClip in listAudioClip)
        {
            if (ClipName.Equals(eachAudioClip.name))
            {
                listAudioSource[audioSourceIndex].loop = IsLoop;
                listAudioSource[audioSourceIndex].clip = eachAudioClip;
            }
            
            if (Delay > 0)
            {
                listAudioSource[audioSourceIndex].PlayDelayed(Delay);
            }
            else
            {
                listAudioSource[audioSourceIndex].Play();
            }
            return;
        }
        Debug.Log("AudioClip not found: " + ClipName);
    }

    public void StopPlaying(string AudioSourceName)
    {
        int audioSourceIndex = -1;
        for (int i = 0; i < listAudioSource.Count; i++)
        {
            if (AudioSourceName == listAudioSource[i].name)
            {
                listAudioSource[i].Stop();
                audioSourceIndex = i;
            }
        }

        if (audioSourceIndex == -1)
        {
            Debug.Log("AudioSource not found: " + AudioSourceName);
        }
    }

    public void InvokePlaySound(string ClipName, string AudioSourceName, bool IsLoop = false, float Delay = 0f)
    {
        StartCoroutine(PlaySoundInvoke(ClipName, AudioSourceName, IsLoop, Delay));
    }

    IEnumerator PlaySoundInvoke(string ClipName, string AudioSourceName, bool IsLoop = false, float Delay = 0f)
    {
        yield return new WaitForSeconds(Delay);
        PlaySound(ClipName, AudioSourceName, IsLoop);
    }
}
